package com.springapp.mvc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author vladimirb
 * @since 3/12/14
 */
@Configuration
@EnableTransactionManagement
@PropertySource("classpath:hibernate.properties")
public class HibernateConfig {

	static final String DRIVER_CLASS_NAME = "com.mysql.jdbc.Driver";

	static final Properties hibernateProperties = new Properties() {
		{
			setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
			setProperty("hibernate.hbm2ddl.auto", "validate");
			setProperty("hibernate.show_sql", "true");
			setProperty("hibernate.current_session_context_class", "org.springframework.orm.hibernate4.SpringSessionContext");
            setProperty("hibernate.connection.CharSet", "utf8");
            setProperty("hibernate.connection.characterEncoding", "utf8");
            setProperty("hibernate.connection.useUnicode", "true");
		}
	};

	@Value("${connection.url}??autoReconnect=true&useUnicode=true&createDatabaseIfNotExist=false&characterEncoding=utf-8")
	String dbUrl;


    @Value("${connection.username}")
	String dbUser;

	@Value("${connection.password}")
	String dbPass;

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		final LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan("com.springapp.mvc.data.entities");
		sessionFactory.setHibernateProperties( hibernateProperties );
		return sessionFactory;
	}

	@Bean
	public HibernateTransactionManager transactionManager() {
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(sessionFactory().getObject());
		return txManager;
	}

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource(dbUrl, dbUser, dbPass);
		dataSource.setDriverClassName( DRIVER_CLASS_NAME );
		return dataSource;
	}
}
