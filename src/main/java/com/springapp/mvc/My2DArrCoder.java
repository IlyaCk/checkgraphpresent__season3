package com.springapp.mvc;

import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ilya
 * Date: 18.04.14
 * Time: 4:30
 * To change this template use File | Settings | File Templates.
 */
@Component
public class My2DArrCoder {
//    private static final Logger LOGGER = Logger.getLogger( My2DArrCoder.class.getName() );

    static public int[][] strTo2DArr(String encodedArr, PropertyResourceBundle propertyResourceBundle) throws IllegalArgumentException {
        String[] linesSeparately = encodedArr.split("rrr");
        int[][] res = new int[linesSeparately.length][];
        for(int i=0; i<res.length; i++) {
            String[] elemsInCurrLine = linesSeparately[i].split("c");
            res[i] = new int[elemsInCurrLine.length];
            for(int j=0; j<res[i].length; j++) {
                try {
                    res[i][j] = new Integer(elemsInCurrLine[j]);
                } catch (NumberFormatException e) {
                    // should be impossible, because should be already checked by javaScript???
                    System.err.println("NAN element was found checking at server-side although it should be checked by javascript at client-side");
//                    LOGGER.log(Level.SEVERE, "NAN element was found checking at server-side although it should be checked by javascript at client-side");
                    throw new IllegalArgumentException(String.format(propertyResourceBundle.getString("verdictExplain.elementNAN"), i+1, j+1));
                }
            }
        }
        return res;
    }

    static public int[][] incidenceMatrixToAdjMatrix(int[][] incidenceMatrix, boolean isDirected, PropertyResourceBundle propertyResourceBundle) throws IllegalArgumentException {
        if(incidenceMatrix.length < 1) {
            System.err.println("Empty matrix was found checking at server-side although it should be checked by javascript at client-side");
//            LOGGER.log(Level.SEVERE, "Empty matrix was found checking at server-side although it should be checked by javascript at client-side");
            throw new IllegalArgumentException(propertyResourceBundle.getString("verdictExplain.matrixCantBeEmpty"));
        }
        int N = incidenceMatrix.length;
        int M = incidenceMatrix[0].length;
        for(int i=1; i<N; i++) {
            if(incidenceMatrix[i].length != M) {
                System.err.println("Too jagged array (non-rectangle, with different lines' length should be already checked by javascript at client-side, but was got by server)");
                throw new IllegalArgumentException(propertyResourceBundle.getString("verdictExplain.tooJaggedArray"));
            }
        }
        int[][] resAdjMatrix = new int[N][N];
        for(int[] row : resAdjMatrix) {
            Arrays.fill(row, 0);
        }
        for(int j=0; j<M; j++) {
            Map<Integer, Set<Integer>> valueToIdx = new HashMap<Integer, Set<Integer>>();
            for(int v = -1; v<=2; v++)
                valueToIdx.put(v, new HashSet<Integer>());
            for(int i=0; i<N; i++) {
                if(incidenceMatrix[i][j] < -1 || incidenceMatrix[i][j] > 2) {
                    throw new IllegalArgumentException(String.format(propertyResourceBundle.getString("verdictExplain.illegalElementOfIncidenceMartix"), i+1, j+1, incidenceMatrix[i][j]));
                }
                valueToIdx.get(incidenceMatrix[i][j]).add(i);
            }
            if(valueToIdx.get(2).size() != 0) { // graph's loop
                if(valueToIdx.get(2).size() != 1 || valueToIdx.get(0).size() != N-1) {
                    throw new IllegalArgumentException(String.format(propertyResourceBundle.getString("verdictExplain.illegalDigitTwoInIncidenceMartix"), j+1));
                }
                int v = ((int) ((Integer[])valueToIdx.get(2).toArray())[0]);
                resAdjMatrix[v][v]++;
                continue; // correct graph's loop;
            }
            // if got here, surely -1, 0, +1 only
            if(isDirected) {
                if(valueToIdx.get(-1).size()!=1 || valueToIdx.get(1).size()!=1 || valueToIdx.get(0).size()!=N-2) {
                    throw new IllegalArgumentException(String.format(propertyResourceBundle.getString("verdictExplain.isntCorrectArc"), j+1));
                }
                int u = valueToIdx.get(-1).toArray(new Integer[1])[0];
                int v = valueToIdx.get(+1).toArray(new Integer[1])[0];
                resAdjMatrix[u][v]++; // good directed edge
            } else { // unDirected
                if(valueToIdx.get(1).size()!=2 || valueToIdx.get(0).size()!=N-2) {
                    throw new IllegalArgumentException(String.format(propertyResourceBundle.getString("verdictExplain.isntCorrectUndirectedEdge"), j+1));
                }
                Integer[] edge = new Integer[2];
                edge = valueToIdx.get(1).toArray(edge);
                int u = edge[0];
                int v = edge[1];
                resAdjMatrix[u][v]++; // good unDirected edge
                resAdjMatrix[v][u]++; // good unDirected edge
            }
        }
        return resAdjMatrix;
    }

    public static int[][] adjListsToAdjMatrix(int[][] adjLists, PropertyResourceBundle propertyResourceBundle) throws IllegalArgumentException {
        if(adjLists.length < 1) {
            System.err.println("Empty adj lists were found checking at server-side although it should be checked by javascript at client-side");
            throw new IllegalArgumentException(propertyResourceBundle.getString("verdictExplain.listsCantBeEmpty"));
        }
        int N = adjLists.length;
        int[][] resAdjMatrix = new int[N][N];
        for(int[] row : resAdjMatrix) {
            Arrays.fill(row, 0);
        }
        for(int i=0; i<N; i++) {
            for(int j=1; j<adjLists[i].length; j++) {
                // loop really start from 1, because a "-1" is added to prevent empty rows
                if(adjLists[i][j]<1 || adjLists[i][j]>N) {
                    throw new IllegalArgumentException(String.format(propertyResourceBundle.getString("verdictExplain.listsValueIllegalForCurrentN"), N, adjLists[i][j]));
                }
                resAdjMatrix[i][adjLists[i][j]-1]++;
            }
        }
        return resAdjMatrix;
    }

    public static String buildDetailedVerdict(int[][] userArray, int[][] etalonAdjMatrix, boolean isDirected, PropertyResourceBundle propertyResourceBundle) {
        if(userArray.length != etalonAdjMatrix.length  ||  userArray[0].length != etalonAdjMatrix[0].length) {
            return propertyResourceBundle.getString("verdictExplain.incorrectVerticeAmount");
        }
        for(int i=0; i<userArray.length; i++) {
            for(int j = 0; j<userArray[i].length; j++) {
                if(userArray[i][j] != etalonAdjMatrix[i][j]) {
                    return String.format(propertyResourceBundle.getString("verdictExplain.incorrectBetweenThisVertice"), i+1, j+1);
                }
            }
        }
        System.err.println("Це дуже дивно, позвіть викладача:\nbuildDetailedVerdict не знаходить конкретної помилки, хоча deepEquals вказала на нерівність");
        return "Це дуже дивно, позвіть викладача:\nbuildDetailedVerdict не знаходить конкретної помилки, хоча deepEquals вказала на нерівність";
    }
}
