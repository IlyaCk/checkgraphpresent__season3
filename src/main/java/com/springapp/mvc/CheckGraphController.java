package com.springapp.mvc;

import com.springapp.mvc.data.DetailedVerdict;
import com.springapp.mvc.data.StudentsSorts;
import com.springapp.mvc.data.SubmitInfo;
import com.springapp.mvc.data.Verdict;
import com.springapp.mvc.data.entities.Graph;
import com.springapp.mvc.data.entities.Student;
import com.springapp.mvc.data.services.GraphService;
import com.springapp.mvc.data.services.OneTimeCodeService;
import com.springapp.mvc.data.services.StudentService;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ilya
 * Date: 09.04.14
 * Time: 12:15
 * To change this template use File | Settings | File Templates.
 */
@Controller
@SessionAttributes({"selectedStudentAsPojo", "isConfirmed", "graph", "allSubmits", "randomOrPreDefined", "language", "propertyResourceBundle"})
public class CheckGraphController {
//    private static final Logger LOGGER = Logger.getLogger( CheckGraphController.class.getName() );

    @Autowired
    StudentService studentService;

    @Autowired
    OneTimeCodeService oneTimeCodeService;

    @Autowired
    GraphService graphService;

    @RequestMapping(value = "/invalidateSession")
    public String invalidateSession(HttpServletRequest req, ModelMap model) {
        if("true".equalsIgnoreCase((String)model.get("isConfirmed"))) {
            try {
                Student theStudent = (Student) model.get("selectedStudentAsPojo");
                oneTimeCodeService.deleteAllByStudentId(theStudent.getId());
//                studentService.clearTemporaryScores(theStudent);
            } catch(NullPointerException e) {
                System.err.println("Smth strange: looks like ``isConfirmed==true'' is already stored in session, but student is not stored");
            }
        }
        model.clear();
        req.getSession().invalidate();
        return "redirect:/";
    }

    @RequestMapping(value = "/")
    public String startPage(ModelMap model) {
        return "chooseLanguage";
    }


    @RequestMapping(value = "/chooseStudent", params = {"language"})
    public String chooseStudPage(@RequestParam(value = "language", required = true) String language,
            ModelMap model) {
        List<Student> allStudents = studentService.getStudents();
        model.addAttribute("allStudents", allStudents);
        List<Integer> allGraphVariants = graphService.getGraphVariantOnly();
        model.addAttribute("allGraphVariants", allGraphVariants);
        model.addAttribute("language", language);
//        System.out.println(System.getProperty("user.dir"));
        model.addAttribute("propertyResourceBundle", (PropertyResourceBundle)ResourceBundle.getBundle("text", new Locale(language)));
        return "chooseStudent";
    }

    @RequestMapping(value = "/generateOneTimeCode", method = RequestMethod.POST, params = {"selectedStudent"})
    @ResponseBody
    public Map generateOneTimeCode(@RequestParam(value = "selectedStudent", required = true) Integer idStudent,
                                      HttpServletRequest request, ModelMap model) {
        oneTimeCodeService.generateAndSave(idStudent, request.getRemoteAddr(), 4);
        Student student = studentService.getStudent(idStudent);
        model.put("selectedStudentAsPojo", student);
        Map result = new HashMap<String, String>();
        result.put("idStudent", student.getId());
        result.put("surname", student.getSurName());
        result.put("firstname", student.getFirstName());
        return result;
    }

    @RequestMapping(value = "/checkOneTimeCodeOne", method = RequestMethod.GET, params = {"selectedStudent", "theCode", "randomOrPreDefined"})
    @ResponseBody
    public Map checkOneTimeCodeOne(
            @RequestParam(value = "selectedStudent", required = true) Integer idStudent,
            @RequestParam(value = "theCode", required = true) Integer codeEntered,
            @RequestParam(value = "randomOrPreDefined", required = true) String randomOrPreDefined,
            ModelMap model
            ) {
        Map result = new HashMap<String, String>();
        try {
            boolean isConfirmed = ("" + codeEntered).length() == 4 &&
                    oneTimeCodeService.checkIfCorrect(idStudent, codeEntered);
            result.put("isConfirmed", "" + isConfirmed);
            model.put("isConfirmed", "" + isConfirmed);
            if (isConfirmed) {
                Student theStudent = (Student) model.get("selectedStudentAsPojo");
                int variant;
                if ("random".equalsIgnoreCase(randomOrPreDefined)) {
                    int prohibitedVariant = theStudent.getVariant();
                    Random rnd = new Random();
                    do {
                        variant = 1 + rnd.nextInt(15);
                    } while (variant == prohibitedVariant);
                } else {
                    variant = theStudent.getVariant();
                }
                model.put("randomOrPreDefined", randomOrPreDefined);
                model.put("graph", graphService.getGraphH(variant));
                Map<String, SubmitInfo> submits = new HashMap<String, SubmitInfo>();
                model.put("allSubmits", submits);
                studentService.clearTemporaryScoresWithLogging(theStudent);
                oneTimeCodeService.deleteAllByStudentId(idStudent); // changed on 20160117
            } else {
                PropertyResourceBundle propertyResourceBundle = (PropertyResourceBundle)model.get("propertyResourceBundle");
                result.put("msg", propertyResourceBundle.getString("serverReply.incorrectCode"));
            }
        } catch (NullPointerException e) {
            System.err.println("WARNING!!! NullPointerException while checking oneTimeCodeOne");
            result.put("isConfirmed", "false");
            result.put("msg", ResourceBundle.getBundle(   "text", new Locale("uk")).getString("serverReply.recheckForCookies") + "");
        }
        return result;
    }



//    @RequestMapping(value = "/setAnonymousVariant")
//    public String setAnonymousVariant (@RequestParam(value = "selectAnonymousVariant") Integer anonVariant,


    @RequestMapping(value = "/setAnonymousVariant", method = RequestMethod.POST, params = {"selectAnonymousVariant"})
    public String setAnonymousVariant (@RequestParam(value = "selectAnonymousVariant", required = true) Integer anonVariant,
                                       ModelMap model) {
        model.put("isConfirmed", "false");
        model.put("selectedStudentAsJson", "");
            model.put("selectedStudentAsPojo", "");
        model.put("graph", graphService.getGraphH(anonVariant));
            Map<String, SubmitInfo> submits = new HashMap<String, SubmitInfo>();
        model.put("allSubmits", submits);
        return "redirectToChoosePresentation";
    }

    @RequestMapping(value = "/afterAnonymous")
    public String setAnonymousVariant (ModelMap model) {
        // TODO: do smth with "/afterAnonymous" versus "/choosePresentation"
        // Unfortunately, I did not find a better way.
        // Advantages : no parameters appear in user-visible URI
        // Disadvantages : address remains "/afterAnonymous" when it should be "/choosePresentation"
        // All redirects and forwards I tried lead to appear parameters and values in user-visible URI
        return "choosePresentation";
    }

    @RequestMapping(value = "/choosePresentation")
    public String choosePresentation(ModelMap model) {
        if(!model.containsKey("graph")) {
            return "redirect:/";
        }
        return "choosePresentation";
    }

    @RequestMapping(value = "/buildThePresentation"/*, params = {"isDirected", "whatPresentation"}*/)
    public String buildThePresentation (
            HttpServletRequest req,
            @RequestParam(value = "isDirected", required = false) String isDirected,
            @RequestParam(value = "whatPresentation", required = false) String whatPresentation,
            ModelMap model) {
        if("GET".equalsIgnoreCase(req.getMethod())) {
            return "forbidden";
        }
        String specifyIsDirectedAndPresentation = isDirected + whatPresentation;
        Map submits = ((Map)model.get("allSubmits"));
        try {
            if (!submits.containsKey(specifyIsDirectedAndPresentation)) {
/*
Creating of SubmitInfo should either just here,
or in some code which is executed in approximately the same time!!!
(a second difference is admissible,  10 seconds is not)
*/
                String randomOrPreDefined = (String) model.get("randomOrPreDefined");
                double scoreFactorForRandom = "random".equalsIgnoreCase(randomOrPreDefined) ? 2 : 1;
                submits.put(specifyIsDirectedAndPresentation, new SubmitInfo(
                        0.75 * scoreFactorForRandom,  // maxScore
                        "random".equalsIgnoreCase(randomOrPreDefined) ? 25 : 40, // what PERCENT is not decreasing due to errors and time wasted
                        0.9,  // reducing factor for each error (unsuccessful submit)
                        0.95, // reducing factor for each minute over standard time
                        whatPresentation.contains("ncidence") ? 6 : 4, // standard time (after which time-based reducing factor is applied)
                        3));  // digits after decimal point
            }
        } catch(NullPointerException e){
            return "forbidden";
        }
        return "buildThePresentation";
    }

    @RequestMapping(value = "/getScoreIfNowOk", method = RequestMethod.POST, params = {"isDirected", "whatPresentation"})
    @ResponseBody
    public Map getScoreIfNowOk(
            @RequestParam(value = "isDirected", required = true) String isDirected,
            @RequestParam(value = "whatPresentation", required = true) String whatPresentation,
            ModelMap model) {
        Map result = new HashMap<String, String>();
        try {
            SubmitInfo submtInf = ((Map<String, SubmitInfo>)model.get("allSubmits")).get(isDirected + whatPresentation);
            result.put("timePassed", submtInf.getTimeSinceStart());
            result.put("timeNorm", submtInf.getMinutesNoPenalty());
            result.put("numBadTries", submtInf.getNumBadTries());
            result.put("scoreForNow", submtInf.calcScore());
            result.put("scoreMaxPossible", submtInf.getMaxScore());
        } catch(NullPointerException e) {
            result.put("sessionBreaked", true);
        }
        return result;
    }

    @RequestMapping(value = "/secretLinkToAddStudents")
    public String secretLinkToAddStudents(HttpServletRequest request, ModelMap model) {
        String remote = request.getRemoteAddr();
        String local = request.getLocalAddr();
        if(remote.equals(local)) {
            /*
            studentService.saveStudent(new Student("Апухтін","Владислав","КМ-14",15));
            studentService.saveStudent(new Student("Білашенко","Владислав","КЕ-14",9));
            studentService.saveStudent(new Student("Біленко","Артем","КМ-14",2));
            studentService.saveStudent(new Student("Білоус","Олексій","КІ-14",3));
            studentService.saveStudent(new Student("Більмаковський","Руслан","КС-14",1));
            studentService.saveStudent(new Student("Бондаренко","Даниіл","КС-14",10));
            studentService.saveStudent(new Student("Борзикін","Дмитро","КМ-14",11));
            studentService.saveStudent(new Student("Боровський","Олександр","КЕ-14",14));
            studentService.saveStudent(new Student("Бородіна","Наталія","КІ-14",6));
            studentService.saveStudent(new Student("Васильченко","Олексій","КМ-14",10));
            studentService.saveStudent(new Student("Виноград","Олександр","КЕ-14",4));
            studentService.saveStudent(new Student("Гасюк","Сергій","КС-14",8));
            studentService.saveStudent(new Student("Глазков","Віктор","КС-14",13));
            studentService.saveStudent(new Student("Голенко","Максим","КІ-14",12));
            studentService.saveStudent(new Student("Гончар","Антон","КМ-14",5));
            studentService.saveStudent(new Student("Данченко","Антон","КЕ-14",7));
            studentService.saveStudent(new Student("Доненко","Валентин","КЕ-14",14));
            studentService.saveStudent(new Student("Захарченко","Павло","КМ-14",5));
            studentService.saveStudent(new Student("Зоря","Віталій","КС-14",8));
            studentService.saveStudent(new Student("Ісаєва","Віталія","КС-14",12));
            studentService.saveStudent(new Student("Коваленко","Костянтин","КЕ-14",3));
            studentService.saveStudent(new Student("Коваленко","Юрій","КІ-14",7));
            studentService.saveStudent(new Student("Коверник","Сергій","КІ-14",2));
            studentService.saveStudent(new Student("Колісник","Богдан","КС-14",9));
            studentService.saveStudent(new Student("Комаров","Артем","КЕ-14",13));
            studentService.saveStudent(new Student("Конак","Віталій","КЕ-14",1));
            studentService.saveStudent(new Student("Коновалюк","Альона","КМ-14",3));
            studentService.saveStudent(new Student("Кузнецов","Андрій","КІ-14",11));
            studentService.saveStudent(new Student("Кузьменко","Арсентій","КІ-14",2));
            studentService.saveStudent(new Student("Кусюмов","Михайло","КЕ-14",9));
            studentService.saveStudent(new Student("Личак","Ігор","КЕ-14",8));
            studentService.saveStudent(new Student("Лістровий","Ярослав","КС-14",15));
            studentService.saveStudent(new Student("Мазур","Сергій","КС-14",15));
            studentService.saveStudent(new Student("Македон","Владислав","КМ-14",4));
            studentService.saveStudent(new Student("МаксИменко","Олег","КЕ-14",6));
            studentService.saveStudent(new Student("Марасін","Данііл","КІ-14",1));
            studentService.saveStudent(new Student("Мельник","Андрій","КМ-14",6));
            studentService.saveStudent(new Student("Мохуренко","Сергій","КС-14",7));
            studentService.saveStudent(new Student("Назаренко","Ярослав","КМ-14",4));
            studentService.saveStudent(new Student("Никифоров","Руслан","КІ-14",14));
            studentService.saveStudent(new Student("Одінцов","Віталій","КС-14",12));
            studentService.saveStudent(new Student("Пархоменко","Микола","КІ-14",13));
            studentService.saveStudent(new Student("Петренко","Ігор","КІ-14",12));
            studentService.saveStudent(new Student("Плахута","Марія","КС-14",13));
            studentService.saveStudent(new Student("Попова","Олена","КС-14",10));
            studentService.saveStudent(new Student("Прокоса","В'ячеслав","КС-14",8));
            studentService.saveStudent(new Student("Рибак","Богдан","КІ-14",15));
            studentService.saveStudent(new Student("Сидорчук","Михайло","КІ-14",7));
            studentService.saveStudent(new Student("Скворцов","Олександр","КІ-14",5));
            studentService.saveStudent(new Student("Снісаренко","Олександр","КС-14",6));
            studentService.saveStudent(new Student("Стефанюк","Богдан","КС-14",2));
            studentService.saveStudent(new Student("ТелевнА","Наталія","КЕ-14",4));
            studentService.saveStudent(new Student("Токарєва","Тетяна","КС-14",5));
            studentService.saveStudent(new Student("Токарський","Артем","КС-14",1));
            studentService.saveStudent(new Student("Федейко","Валерія","КМ-14",11));
            studentService.saveStudent(new Student("Фуженко","Ігор","КЕ-14",3));
            studentService.saveStudent(new Student("Черак","Назар","КС-14",14));
            studentService.saveStudent(new Student("Чиженко","Євгеній","КІ-14",10));
            studentService.saveStudent(new Student("Шаповалова","Анастасія","КМ-14",6));
            studentService.saveStudent(new Student("Шляга","Павло","КС-14",11));
            studentService.saveStudent(new Student("Яровий","Віталій","КС-14",1));
            */

            /*
            studentService.saveStudent(new Student("Антонюк","Олександр","КС-15",14));
            studentService.saveStudent(new Student("Бабко","Руслан","КМ-15",13));
            studentService.saveStudent(new Student("Березовський","Максим","КЕ-15",7));
            studentService.saveStudent(new Student("Брик","Сергій","КЕ-15",9));
            studentService.saveStudent(new Student("Буданцева","Дарина","КС-15",1));
            studentService.saveStudent(new Student("Бурдонос","Олександр","КІ-15",6));
            studentService.saveStudent(new Student("Галицький","Данило","КМ-15",8));
            studentService.saveStudent(new Student("Головатий","Олександр","КІ-15",15));
            studentService.saveStudent(new Student("Гончаренко","Вероніка","КЕ-15",11));
            studentService.saveStudent(new Student("Горб","Антон","КІ-15",5));
            studentService.saveStudent(new Student("Грекало","Аліна","КЕ-15",15));
            studentService.saveStudent(new Student("Грипак","Іван","КМ-15",8));
            studentService.saveStudent(new Student("Гриценко","Вікторія","КІ-15",6));
            studentService.saveStudent(new Student("Гришко","Сергій","КС-15",12));
            studentService.saveStudent(new Student("Дем`яненко","Анастасія","КІ-15",13));
            studentService.saveStudent(new Student("Дем`яненко","Артем","КЕ-15",11));
            studentService.saveStudent(new Student("Кириченко","Дар`я","КМ-15",7));
            studentService.saveStudent(new Student("Кісаров","Богдан","КІ-15",2));
            studentService.saveStudent(new Student("Кішка","Святослав","КС-15",10));
            studentService.saveStudent(new Student("Колібабчук","Ірина","КЕ-15",7));
            studentService.saveStudent(new Student("Кошик","Ростислав","КМ-15",6));
            studentService.saveStudent(new Student("Кравченко","Олександр","КІ-15",1));
            studentService.saveStudent(new Student("Крикун","Віталій","КМ-15",12));
            studentService.saveStudent(new Student("Кручиненко","Олеся","КС-15",10));
            studentService.saveStudent(new Student("Кучеренко","Денис","КС-15",10));
            studentService.saveStudent(new Student("Махиня","Олександр","КІ-15",14));
            studentService.saveStudent(new Student("Мельничук","Олексій","КС-15",3));
            studentService.saveStudent(new Student("Мигаль","Альона","КІ-15",14));
            studentService.saveStudent(new Student("Мірошник","Павло","КІ-15",11));
            studentService.saveStudent(new Student("Пономаренко","Максим","КС-15",3));
            studentService.saveStudent(new Student("Пясецький","Давид","КІ-15",2));
            studentService.saveStudent(new Student("Рідкий","Дмитро","КС-15",4));
            studentService.saveStudent(new Student("Родній","Олександр","КС-15",9));
            studentService.saveStudent(new Student("Самойлик","Іван","КЕ-15",4));
            studentService.saveStudent(new Student("Ситник","Максим","КІ-15",3));
            studentService.saveStudent(new Student("Сізов","Олександр","КС-15",7));
            studentService.saveStudent(new Student("Сліпченко","Катерина","КМ-15",9));
            studentService.saveStudent(new Student("Суботін","Микита","КЕ-15",8));
            studentService.saveStudent(new Student("Теплий","Михайло","КЕ-15",13));
            studentService.saveStudent(new Student("Тимофеєв","Владислав","КМ-15",14));
            studentService.saveStudent(new Student("Томілін","Микита","КМ-15",15));
            studentService.saveStudent(new Student("Трохименко","Владислав","КС-15",12));
            studentService.saveStudent(new Student("Упир","Ольга","КС-15",5));
            studentService.saveStudent(new Student("Харченко","Василь","КС-15",1));
            studentService.saveStudent(new Student("Черниш","Олександр","КС-15",9));
            studentService.saveStudent(new Student("Чумаченко","Владислав","КЕ-15",5));
            studentService.saveStudent(new Student("Шаповал","Віталій","КІ-15",6));
            studentService.saveStudent(new Student("Шаповал","Олег","КС-15",8));
            studentService.saveStudent(new Student("Шевченко","Катерина","КІ-15",5));
            studentService.saveStudent(new Student("Шинкаренко","Валерія","КМ-15",3));
            studentService.saveStudent(new Student("Шкуренко","Анастасія","КС-15",1));
            studentService.saveStudent(new Student("Явник","Олексій","КС-15",2));
            studentService.saveStudent(new Student("Яцун","Ксенія","КІ-15",2));
            */

/*
            studentService.saveStudent(new Student("Tariq","Ahmed","???-16",14));
            studentService.saveStudent(new Student("Аль-Савах","Маяда","КС-16",5));
            studentService.saveStudent(new Student("Бахчагулян","Вазген","КС-16",5));
            studentService.saveStudent(new Student("Безпоясний","Олександр","КІ-16",9));
            studentService.saveStudent(new Student("Безрук","Олександр","КС-16",14));
            studentService.saveStudent(new Student("Бойко","Денис","КС-16",4));
            studentService.saveStudent(new Student("Галасун","Руслан","КЕ-16",15));
            studentService.saveStudent(new Student("Гусак","Володимир","КС-16",15));
            studentService.saveStudent(new Student("Данилюк","Роман","КС-16",7));
            studentService.saveStudent(new Student("Дем'яненко","Артем","КЕ-16",5));
            studentService.saveStudent(new Student("Ємець","Віталій","КІ-16",2));
            studentService.saveStudent(new Student("Захарченко","Дмітрій","КІ-16",10));
            studentService.saveStudent(new Student("Кириленко","Ярослав","КС-16",8));
            studentService.saveStudent(new Student("Ковальчук","Богдан","КС-16",2));
            studentService.saveStudent(new Student("Ковтун","Катерина","КС-16",7));
            studentService.saveStudent(new Student("Кожевнікова","Єлизавета","КЕ-16",13));
            studentService.saveStudent(new Student("Козєй","Андрій","КС-16",13));
            studentService.saveStudent(new Student("Колбовський","Борис","КМ-16",12));
            studentService.saveStudent(new Student("Костенко","Дмитро","КС-16",12));
            studentService.saveStudent(new Student("Кравченко","Едуард","КС-16",11));
            studentService.saveStudent(new Student("Краснощок","Богдан","КІ-16",3));
            studentService.saveStudent(new Student("Круглов","Артур","КІ-16",2));
            studentService.saveStudent(new Student("Кудряченко","Віктор","КС-16",5));
            studentService.saveStudent(new Student("Купневський","Артем","КС-16",15));
            studentService.saveStudent(new Student("Лебедєв","Віталій","КС-16",2));
            studentService.saveStudent(new Student("Лілєйкіте","Яна","КЕ-16",6));
            studentService.saveStudent(new Student("Лобода","Аліна","КМ-16",15));
            studentService.saveStudent(new Student("Масик","Олександр","КМ-16",10));
            studentService.saveStudent(new Student("Матвіїва","Оксана","КМ-16",4));
            studentService.saveStudent(new Student("Міхальченко","Антон","КС-16",12));
            studentService.saveStudent(new Student("Мостовий","Ігор","КІ-16",7));
            studentService.saveStudent(new Student("Надточій","Дмитро","КІ-16",14));
            studentService.saveStudent(new Student("Пилипенко","Олег","КС-16",8));
            studentService.saveStudent(new Student("Поштаренко","Назар","КС-16",9));
            studentService.saveStudent(new Student("Ролько","Олександр","КІ-16",9));
            studentService.saveStudent(new Student("Ряжен","Віктор","КІ-16",1));
            studentService.saveStudent(new Student("Савост'янова","Ірина","КМ-16",11));
            studentService.saveStudent(new Student("Сидоров","Василь","КС-16",4));
            studentService.saveStudent(new Student("Смиковський","Сергій","КІ-16",7));
            studentService.saveStudent(new Student("Смірнов","Ігор","КС-16",6));
            studentService.saveStudent(new Student("Стрілець","Євгеній","КС-16",10));
            studentService.saveStudent(new Student("Таран","Гліб","КМ-16",13));
            studentService.saveStudent(new Student("Теліженко","Дмитро","КС-16",8));
            studentService.saveStudent(new Student("Фудько","Павло","КС-16",1));
            studentService.saveStudent(new Student("Хупченко","Тетяна","КС-16",1));
            studentService.saveStudent(new Student("Чернов","Семен","КМ-16",12));
            studentService.saveStudent(new Student("Чорний","Едуард","КМ-16",11));
            studentService.saveStudent(new Student("Швець","Вікторія","КС-16",3));
            studentService.saveStudent(new Student("Шелест","Максим","КС-16",9));
            studentService.saveStudent(new Student("Шляховський","Олег","КМ-16",3));
            studentService.saveStudent(new Student("Якубенко","Віталій","КС-16",1));
            studentService.saveStudent(new Student("Яременко","Артем","КЕ-16",6));
            studentService.saveStudent(new Student("а1","а1","???-16",6));
            studentService.saveStudent(new Student("а2","а2","???-16",8));
            */

            /*
            studentService.saveStudent(new Student("Антонюк","Олександр","КС-171",6));
            studentService.saveStudent(new Student("Багмет","Анатолій","КС-171",1));
            studentService.saveStudent(new Student("Березовець","Дмитро","КС-171",9));
            studentService.saveStudent(new Student("Буряченко","Михайло","КС-171",14));
            studentService.saveStudent(new Student("Грушовий","Владислав","КС-171",15));
            studentService.saveStudent(new Student("Гутьман","Іван","КС-171",9));
            studentService.saveStudent(new Student("Дробаха","Дмитро","КС-171",5));
            studentService.saveStudent(new Student("Запорожець","Ілля","КС-171",13));
            studentService.saveStudent(new Student("Зеленков","Антон","КС-171",5));
            studentService.saveStudent(new Student("Івжич","Олексій","КС-171",8));
            studentService.saveStudent(new Student("Карабань","Нікіта","КС-171",15));
            studentService.saveStudent(new Student("Каратнюк","Владислав","КС-171",12));
            studentService.saveStudent(new Student("Крижановський","Олександр","КС-171",7));
            studentService.saveStudent(new Student("Кудряченко","Віктор","КС-171",9));
            studentService.saveStudent(new Student("Лимар","Олександр","КС-171",4));
            studentService.saveStudent(new Student("Макалюк","Максим","КС-171",3));
            studentService.saveStudent(new Student("Манукян","Ян","КС-171",4));
            studentService.saveStudent(new Student("Мауріна","Олена","КС-171",12));
            studentService.saveStudent(new Student("Мисюра","Юлія","КС-171",15));
            studentService.saveStudent(new Student("Мормуль","Олександр","КС-171",10));
            studentService.saveStudent(new Student("Нечаєнко","Олексій","КС-171",2));
            studentService.saveStudent(new Student("Нікітюк","Владислав","КС-171",14));
            studentService.saveStudent(new Student("Носков","Єгор","КС-171",1));
            studentService.saveStudent(new Student("Олексієнко","Володимир","КС-171",8));
            studentService.saveStudent(new Student("Письменний","Іван","КС-171",13));
            studentService.saveStudent(new Student("Скляр","Максим","КС-171",11));
            studentService.saveStudent(new Student("Труба","Катерина","КС-171",7));
            studentService.saveStudent(new Student("Черненко","Дарія","КС-171",10));
            studentService.saveStudent(new Student("Чунарьов","Олександр","КС-171",6));
            studentService.saveStudent(new Student("Бойко","Олег","КС-172",14));
            studentService.saveStudent(new Student("Гущин","Владислав","КС-172",4));
            studentService.saveStudent(new Student("Іванов","Микита","КС-172",8));
            studentService.saveStudent(new Student("Міщенко","Андрій","КС-172",10));
            studentService.saveStudent(new Student("Мосінзовий","Руслан","КС-172",2));
            studentService.saveStudent(new Student("Оношко","Максім","КС-172",4));
            studentService.saveStudent(new Student("Фендюр","Владислав","КС-172",8));
            studentService.saveStudent(new Student("Хандюк","Володимир","КС-172",12));
            studentService.saveStudent(new Student("Цвєтков","Ілля","КС-172",12));
            studentService.saveStudent(new Student("Цимбал","Аліна","КС-172",11));
            studentService.saveStudent(new Student("Чернієнко","Дмитро","КС-172",13));
            studentService.saveStudent(new Student("Шевченко","Анастасія","КС-172",13));
            studentService.saveStudent(new Student("Шелест","Артем","КС-172",8));
            studentService.saveStudent(new Student("Яцун","Сергій","КС-172",2));
            studentService.saveStudent(new Student("Євстаф'єва","Олександра","КІ-17",7));
            studentService.saveStudent(new Student("Засядько","Катерина","КІ-17",1));
            studentService.saveStudent(new Student("Ільченко","Дмітрій","КІ-17",4));
            studentService.saveStudent(new Student("Назаренко","Євгеній","КІ-17",9));
            studentService.saveStudent(new Student("Прямухіна","Олена-Марія","КІ-17",2));
            studentService.saveStudent(new Student("Цвіркун","Руслан","КІ-17",5));
            studentService.saveStudent(new Student("Комарова","Ірина","КЕ-17",10));
            studentService.saveStudent(new Student("Луцик","Едуард","КЕ-17",11));
            studentService.saveStudent(new Student("Пільгун","Іван","КЕ-17",6));
            studentService.saveStudent(new Student("Шевченко","Дмитро","КЕ-17",11));
            studentService.saveStudent(new Student("Басенко","Олексій","КМ-17",10));
            studentService.saveStudent(new Student("Завіновський","Владислав","КМ-17",6));
            studentService.saveStudent(new Student("Савченко","Володимир","КМ-17",9));
            studentService.saveStudent(new Student("Ященко","Данііл","КМ-17",1));
            studentService.saveStudent(new Student("Масик","Олександр","КМ-17",3));
            studentService.saveStudent(new Student("Бурлай","Максим","КТ-17",15));
            studentService.saveStudent(new Student("Гунченко","Костянтин","КТ-17",5));
            studentService.saveStudent(new Student("Давиденко","Дмитро","КТ-17",7));
            studentService.saveStudent(new Student("Яструб","Дмитро","КТ-17",14));
            */

            /*
            studentService.saveStudent(new Student("Aqib Muhammad", "", "CS-17",1));
            studentService.saveStudent(new Student("Dodzi Stella Bella-bello", "", "CS-17",2));
            studentService.saveStudent(new Student("Conteh Lamin", "", "CS-17",3));
            studentService.saveStudent(new Student("Njai Muhammed", "", "CS-17",4));
            studentService.saveStudent(new Student("Faal Daht", "", "CS-17",5));
            studentService.saveStudent(new Student("Fatty Alagie", "", "CS-17",6));
            studentService.saveStudent(new Student("Keita Oumar", "", "CS-17",7));
            studentService.saveStudent(new Student("Sea Salomon", "", "CS-17",8));
            studentService.saveStudent(new Student("Amarfio Dereck", "", "CS-17",9));
            studentService.saveStudent(new Student("Cherno Dawda Sowe", "", "CS-17",10));
            studentService.saveStudent(new Student("Mustafa Ghulam", "", "CS-17",11));
            studentService.saveStudent(new Student("Quaye Roland Kwesi", "", "CS-17",12));
            */

            /*
            studentService.saveStudent(new Student("Білик","Владислав","КС-181",1));
            studentService.saveStudent(new Student("Боженко","Владислав","КС-181",2));
            studentService.saveStudent(new Student("Бойко","Сергій","КС-181",3));
            studentService.saveStudent(new Student("Бондаренко","Валерія","КТ-18",4));
            studentService.saveStudent(new Student("Бордюгов","Микита","КС-181",5));
            studentService.saveStudent(new Student("Браславець","Володимир","КТ-18",6));
            studentService.saveStudent(new Student("Бухало","Максим","КТ-18",7));
            studentService.saveStudent(new Student("Вакулінський","Олександр","КІ-18",8));
            studentService.saveStudent(new Student("Весельський","Олексій","КС-181",9));
            studentService.saveStudent(new Student("Голик","Роман","КС-181",10));
            studentService.saveStudent(new Student("Губар","Назар","КТ-18",11));
            studentService.saveStudent(new Student("Гулак","Максим","КТ-18",12));
            studentService.saveStudent(new Student("Декаленко","Андрій","КТ-18",13));
            studentService.saveStudent(new Student("Донченко","Владислав","КС-181",14));
            studentService.saveStudent(new Student("Зачешигрива","Богдан","КС-181",15));
            studentService.saveStudent(new Student("Івжич","Олексій","КС-181",1));
            studentService.saveStudent(new Student("Кайданський","Артур","КС-182",2));
            studentService.saveStudent(new Student("Князєв","Радомир","КС-181",3));
            studentService.saveStudent(new Student("Колесник","Максим","КЕ-18",4));
            studentService.saveStudent(new Student("Коркоць","Олександр","КС-181",5));
            studentService.saveStudent(new Student("Куротченко","Віталій","КС-181",6));
            studentService.saveStudent(new Student("Лесечко","Денис","КС-181",7));
            studentService.saveStudent(new Student("Липоватий","Нікіта","КС-181",8));
            studentService.saveStudent(new Student("Литвиненко","Жанна","КІ-18",9));
            studentService.saveStudent(new Student("Макаренко","Дар'я","КС-181",10));
            studentService.saveStudent(new Student("Мельник","Наталія","КІ-18",11));
            studentService.saveStudent(new Student("Митренко","Вадим","КМ-18",12));
            studentService.saveStudent(new Student("Моісєєв","Дмитро","КТ-18",13));
            studentService.saveStudent(new Student("Носенко","Нікіта","КС-181",14));
            studentService.saveStudent(new Student("Панасюк","Вадим","КТ-18",15));
            studentService.saveStudent(new Student("Пахаренко","Ігор","КС-181",1));
            studentService.saveStudent(new Student("Пересунько","Сергій","КС-181",2));
            studentService.saveStudent(new Student("Пожар","Сергій","КТ-18",3));
            studentService.saveStudent(new Student("Пономаренко","Євгеній","КТ-18",4));
            studentService.saveStudent(new Student("Похитун","Владислав","КС-181",5));
            studentService.saveStudent(new Student("Роман","Денис","КС-181",6));
            studentService.saveStudent(new Student("Романенко","Владислав","КС-181",7));
            studentService.saveStudent(new Student("Сало","Тамара","КІ-18",8));
            studentService.saveStudent(new Student("Сапейко","Максим","КС-181",9));
            studentService.saveStudent(new Student("Семенченко","Володимир","КС-181",10));
            studentService.saveStudent(new Student("Стеценко","Артур","КТ-18",11));
            studentService.saveStudent(new Student("Ткаченко","Євгеній","КІ-18",12));
            studentService.saveStudent(new Student("Фендюр","Владислав","КС-181",13));
            studentService.saveStudent(new Student("Чернишенко","Денис","КТ-18",14));
            studentService.saveStudent(new Student("Шаблевський","Нікіта","КС-181",15));
            */





            //      search : ([А-я]+)\t([А-я]+)\t([А-я\-\d]+)\t(\d+)
            //      search : ([А-яІіЄє'\-]+)\t([А-яІіЄє' \-]+)\t([А-яІіЄє\-\d]+)\t(\d+)
            //      replace : studentService.saveStudent(new Student("$1","$2","$3",$4));
            return "redirect:/invalidateSession";
        } else {
            return "forbidden";
        }
    }

    void addSheetToExcelBook(Workbook workbook, String sheetName, List<Student> students, Comparator<Student> cmp) {
        Sheet sheet = workbook.createSheet(sheetName);
        Collections.sort(students, cmp);
        Row headRow = sheet.createRow(0);
        headRow.createCell(0).setCellValue("id у БД");
        headRow.createCell(1).setCellValue("Прізвище");
        headRow.createCell(2).setCellValue("Ім'я");
        headRow.createCell(3).setCellValue("Група");
        headRow.createCell(4).setCellValue("Сума балів");
        headRow.createCell(5).setCellValue("БалНеорСпСум");
        headRow.createCell(6).setCellValue("БалНеорМатрСум");
        headRow.createCell(7).setCellValue("БалНеорМатрІнц");
        headRow.createCell(8).setCellValue("БалОрСпСум");
        headRow.createCell(9).setCellValue("БалОрМатрСум");
        headRow.createCell(10).setCellValue("БалОрМатрІнц");
        for(int idx = 0; idx < students.size(); idx++) {
            Row row = sheet.createRow(idx + 1);
            row.createCell(0).setCellValue((double)students.get(idx).getId());
            row.createCell(1).setCellValue(students.get(idx).getSurName());
            row.createCell(2).setCellValue(students.get(idx).getFirstName());
            row.createCell(3).setCellValue(students.get(idx).getGrp());
            row.createCell(4).setCellValue(students.get(idx).getTotalScore());
            row.createCell(5).setCellValue(students.get(idx).getUnDirectedAdjListsScore());
            row.createCell(6).setCellValue(students.get(idx).getUnDirectedAdjMatrixScore());
            row.createCell(7).setCellValue(students.get(idx).getUnDirectedIncidenceMatrixScore());
            row.createCell(8).setCellValue(students.get(idx).getYesDirectedAdjListsScore());
            row.createCell(9).setCellValue(students.get(idx).getYesDirectedAdjMatrixScore());
            row.createCell(10).setCellValue(students.get(idx).getYesDirectedIncidenceMatrixScore());
        }
    }

    @RequestMapping(value = "/downloadAllResults", method = RequestMethod.GET)
    public void downloadAllResults (HttpServletRequest request, HttpServletResponse response) {
        List<Student> allStudentsResults = studentService.getStudents();
        Workbook resBook = new HSSFWorkbook();
        addSheetToExcelBook(resBook, "Id", allStudentsResults, StudentsSorts.byId);
        addSheetToExcelBook(resBook, "Бали", allStudentsResults, StudentsSorts.byScore);
        addSheetToExcelBook(resBook, "Прізвища", allStudentsResults, StudentsSorts.byName);
        addSheetToExcelBook(resBook, "Групи", allStudentsResults, StudentsSorts.byGroup);
        try {
            response.setContentType("application/vnd.ms-excel");
            BufferedOutputStream outToClient = new BufferedOutputStream(response.getOutputStream());
            Thread.sleep(2000);
            resBook.write(outToClient);
            resBook.close();
            outToClient.close();
            System.out.println("All results have been downloaded at IP = " + request.getRemoteAddr());
        } catch (IOException | InterruptedException e) {
//            e.printStackTrace();
            System.out.println("Exception : class " + e.getClass().toString() + " // message " + e.getMessage() );
        }
    }

    @RequestMapping(value = "/theMainCheckStage", method = RequestMethod.POST, params = {"isDirected", "whatPresentation", "presentedByUser", "doShowErrorDetails"})
    @ResponseBody
    public Map theMainCheckStage (
            @RequestParam(value = "isDirected", required = true) String isDirected,
            @RequestParam(value = "whatPresentation", required = true) String whatPresentation,
            @RequestParam(value = "presentedByUser", required = true) String presentedByUser,
            @RequestParam(value = "doShowErrorDetails", required = false) String errorIsDetailed,
            HttpServletRequest request,
            ModelMap model) {
        Map result = new HashMap<String, String>();

        PropertyResourceBundle propertyResourceBundle = (PropertyResourceBundle)model.get("propertyResourceBundle");
        if(propertyResourceBundle == null) {
            System.err.println("propertyResourceBundle is null where it should be already set");
            propertyResourceBundle = (PropertyResourceBundle)ResourceBundle.getBundle("text", new Locale("uk"));
            model.addAttribute("propertyResourceBundle", propertyResourceBundle);
        }

        int[][] etalonAdjMatrix = My2DArrCoder.strTo2DArr("true".equalsIgnoreCase(isDirected) ?
                ((Graph)model.get("graph")).getDirectedEthaloneAnswer() :
                ((Graph)model.get("graph")).getUnDirectedEthaloneAnswer(),
                propertyResourceBundle);

        SubmitInfo submtInf = ((Map<String, SubmitInfo>)(model.get("allSubmits"))).get(isDirected + whatPresentation);
        DetailedVerdict detailedVerdict = graphService.checkCorrectness(isDirected, whatPresentation, etalonAdjMatrix, presentedByUser, submtInf, propertyResourceBundle);
        result.put("verdict", detailedVerdict.getMainVerdict());
        System.out.println(detailedVerdict.getMainVerdict());
        if("true".equalsIgnoreCase(errorIsDetailed)) {
            submtInf.oneMoreTryWasted();
            result.put("detailedExplain", detailedVerdict.getDetailedExplain());
        } else {
            result.put("detailedExplain", propertyResourceBundle.getString("verdictExplain.notShown"));
        }
        String isConfirmed = (String)model.get("isConfirmed");
        result.put("isConfirmed", isConfirmed);

        if(detailedVerdict.getMainVerdict().equals(Verdict.OK)) {
            double scoreNew = detailedVerdict.getScore();
            result.put("scoreMaxPossible", submtInf.getMaxScore());
            result.put("scoreNew", scoreNew);

            if("true".equalsIgnoreCase(isConfirmed)) {
                int idStudent = ((Student)model.get("selectedStudentAsPojo")).getId();
                double scoreOldNotProved = studentService.loadScore(idStudent, isDirected, whatPresentation, false);
                double scoreOldProved = studentService.loadScore(idStudent, isDirected, whatPresentation, true);
                result.put("scoreOldNotProved", scoreOldNotProved);
                result.put("scoreOldProved", scoreOldProved);
                if(scoreNew > scoreOldNotProved) {
                    studentService.saveScore(idStudent, isDirected, whatPresentation, false, scoreNew, scoreOldNotProved, request.getRemoteAddr());
                }
                model.addAttribute("selectedStudentAsPojo", studentService.getStudent(idStudent));
                // re-load is required for correct view of current score
            }
        }
        return result;
    }

    @RequestMapping(value = "/startSaveCurrScore", method = RequestMethod.POST)
    public String startSaveCurrScore(HttpServletRequest request, ModelMap model) {
        if("true".equalsIgnoreCase((String)model.get("isConfirmed")) && model.containsKey("selectedStudentAsPojo")) {
            oneTimeCodeService.generateAndSave(((Student)model.get("selectedStudentAsPojo")).getId(), request.getRemoteAddr(), 5);
            return "saveCurrScore";
        } else {
            return "forbidden";
        }
    }

    @RequestMapping(value = "/checkOneTimeCodeTwo", method = RequestMethod.GET, params = {"theCode"})
    @ResponseBody
    public Map checkOneTimeCodeTwo(
            @RequestParam(value = "theCode", required = true) Integer codeEntered,
            HttpServletRequest request, ModelMap model)  {
        Map result = new HashMap<String, String>();
        try {
            Student theStudent = (Student)model.get("selectedStudentAsPojo");
            boolean isConfirmed = ("" + codeEntered).length() == 5   &&   theStudent != null   &&
                    oneTimeCodeService.checkIfCorrect(theStudent.getId(), codeEntered);
            result.put("isConfirmed", "" + isConfirmed);
            if (isConfirmed) {
                try {
                    result.put("changes", studentService.makeTemporaryScoresPermanent(theStudent, request.getRemoteAddr()));
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                oneTimeCodeService.deleteAllByStudentId(theStudent.getId());
                studentService.clearTemporaryScores(theStudent);
            } else {
                PropertyResourceBundle propertyResourceBundle = (PropertyResourceBundle)model.get("propertyResourceBundle");
                result.put("msg", propertyResourceBundle.getString("serverReply.incorrectCode"));
            }
        } catch (NullPointerException e) {
            System.err.println("WARNING!!! NullPointerException while checking oneTimeCodeTwo");
            result.put("isConfirmed", "false");
            result.put("msg", ResourceBundle.getBundle(   "text", new Locale("uk")).getString("serverReply.recheckForCookies") + " oneTimeCodeTwo");
        }
        return result;
    }

}
