package com.springapp.mvc;

import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: ilya
 * Date: 15.04.14
 * Time: 11:34
 * To change this template use File | Settings | File Templates.
 */
@Component
public class GenOneTimeCode {
    static Random rnd = new Random();
    public static int genNew4() {
        return rnd.nextInt(9000) + 1000;
    }
    public static int genNew5() {
        return rnd.nextInt(90000) + 10000;
    }
}
