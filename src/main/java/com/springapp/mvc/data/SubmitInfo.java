package com.springapp.mvc.data;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: ilya
 * Date: 18.04.14
 * Time: 14:13
 * To change this template use File | Settings | File Templates.
 */
public class SubmitInfo {
    private int numBadTries;
    private long timeStartedAt;

    public double getNonDecreasingPercent() {
        return nonDecreasingPercent;
    }

    private double nonDecreasingPercent;
    private double maxScore;
    private double factorEachError;
    private double factorEachMinute;
    private int minutesNoPenalty;
    private int numDigsAfterPoint;

    static DecimalFormat formatter = new DecimalFormat();

    public SubmitInfo(double maxScore, double nonDecreasingPercent, double factorEachError, double factorEachMinute, int minutesNoPenalty, int numDigsAfterPoint) {
        this.maxScore = maxScore;
        this.nonDecreasingPercent = nonDecreasingPercent;
        this.factorEachError = factorEachError;
        this.factorEachMinute = factorEachMinute;
        this.minutesNoPenalty = minutesNoPenalty;
        this.numDigsAfterPoint = numDigsAfterPoint;
        numBadTries = 0;
        timeStartedAt = Calendar.getInstance().getTimeInMillis();
    }

    public void oneMoreTryWasted () {
        numBadTries++;
    }

    public int getNumBadTries() {
        return numBadTries;
    }

    public int getMinutesNoPenalty() {
        return minutesNoPenalty;
    }

    public double getMaxScore() {
        return maxScore;
    }

    public long getTimeSinceStart() { // in milliSeconds
        return Calendar.getInstance().getTimeInMillis() - timeStartedAt             ;
    }

    public double calcScore() {
        formatter.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
        formatter.setMaximumFractionDigits(numDigsAfterPoint);
        return new Double(formatter.format(maxScore * (nonDecreasingPercent/100.0 +
                (100 - nonDecreasingPercent)/100.0 * Math.pow(factorEachError, this.numBadTries) *
                Math.pow(factorEachMinute, Math.max(0.0, getTimeSinceStart() / 60000.0 - minutesNoPenalty)))));
    }
};
