package com.springapp.mvc.data;

import com.springapp.mvc.data.entities.Student;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ilya on 21.03.2015.
 */
public class StudentsSorts {
    final static Collator collator = Collator.getInstance(Locale.forLanguageTag("uk"));
    public final static Comparator<Student> byId = new Comparator<Student>() {
        @Override
        public int compare(Student o1, Student o2) {
            return o1.getId() - o2.getId();
        }
    };
    public final static Comparator<Student> byScore = new Comparator<Student>() {
        @Override
        public int compare(Student o1, Student o2) {
            double score1 = o1.getTotalScore();
            double score2 = o2.getTotalScore();
            if(score1 < score2 - 1e-6) {
                return +1;
            } else if(score1 > score2 + 1e-6) {
                return -1;
            }
            return 0;
        }
    };
    public final static Comparator<Student> byName = new Comparator<Student>() {
        @Override
        public int compare(Student o1, Student o2) {
            if(!o1.getSurName().equals(o2.getSurName())) {
                return collator.compare(o1.getSurName(), o2.getSurName());
            } else if(!o1.getFirstName().equals(o2.getFirstName())) {
                return collator.compare(o1.getFirstName(), o2.getFirstName());
            } else {
                return o1.getId() - o2.getId();
            }
        }
    };

    static Pattern getYearDigsPattern = Pattern.compile("[^\\d](\\d\\d)\\d?");
    static Pattern getSpecPattern = Pattern.compile("^([^\\d\\-]+)\\-\\d{2,3}");
    static Pattern getGroupOfSameSpecPattern = Pattern.compile("[^\\d]\\d\\d(\\d?)");

    public final static Comparator<Student> byGroup = new Comparator<Student>() {
        @Override
        public int compare(Student stud1, Student stud2) {
            String grp1 = stud1.getGrp();
            String grp2 = stud2.getGrp();
            Matcher matcherYear1 = getYearDigsPattern.matcher(grp1);
            String year1 = matcherYear1.find() ? matcherYear1.group(matcherYear1.groupCount()) : "";
            Matcher matcherYear2 = getYearDigsPattern.matcher(grp2);
            String year2 = matcherYear2.find() ? matcherYear2.group(matcherYear2.groupCount()) : "";
            if(!year1.equals(year2)) {
                return year2.compareTo(year1);
            }
            Matcher matcherSpec1 = getSpecPattern.matcher(grp1);
            Matcher matcherSpec2 = getSpecPattern.matcher(grp2);
            String spec1 = matcherSpec1.find() ? matcherSpec1.group(1) : "__";
            String spec2 = matcherSpec2.find() ? matcherSpec2.group(1) : "__";
            String[] groupsOrder = {"__", "КС", "КІ", "КЕ", "КМ", "КТ", "CS"};
            if(!spec1.equals(spec2)) {
                for (int i = 0; i < groupsOrder.length; i++) {
                    if (spec1.startsWith(groupsOrder[i])) {
                        return -1;
                    } else if (spec2.startsWith(groupsOrder[i])) {
                        return +1;
                    }
                }
            } else if(!grp1.equals(grp2)) { // already found that same speciality, so it's smth alike "KC171" versus "KC172"
                return grp1.compareTo(grp2);
            } else if(!stud1.getSurName().equals(stud2.getSurName())) {
                return collator.compare(stud1.getSurName(), stud2.getSurName());
            } else if(!stud1.getFirstName().equals(stud2.getFirstName())) {
                return collator.compare(stud1.getFirstName(), stud2.getFirstName());
            }
            return stud1.getId() - stud2.getId(); // actually -- just to avoid warning
        }
    };
}

