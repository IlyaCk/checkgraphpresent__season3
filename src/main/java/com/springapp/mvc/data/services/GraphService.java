package com.springapp.mvc.data.services;

import com.springapp.mvc.My2DArrCoder;
import com.springapp.mvc.data.DetailedVerdict;
import com.springapp.mvc.data.SubmitInfo;
import com.springapp.mvc.data.Verdict;
import com.springapp.mvc.data.entities.Graph;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.PropertyResourceBundle;

/**
 * Created with IntelliJ IDEA.
 * User: ilya
 * Date: 10.04.14
 * Time: 21:47
 * To change this template use File | Settings | File Templates.
 */
@Repository
@Transactional
public class GraphService {
//    private static final Logger LOGGER = Logger.getLogger( GraphService.class.getName() );

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    StudentService studentService;

    public void saveGraph(Graph graph) {
        sessionFactory.getCurrentSession().saveOrUpdate(graph);
    }

    public Graph getGraph(Integer id) {
        return (Graph) sessionFactory.getCurrentSession().get(Graph.class, id);
    }

    public List<Graph> getGraphs() {
        return sessionFactory.getCurrentSession()
                .createCriteria(Graph.class)
                .list();
    }

    public Graph getGraphH(Integer variant) {
        Criteria ggc = sessionFactory.getCurrentSession().createCriteria(Graph.class);
        ggc.add(Restrictions.eq("variant", variant));
        return (Graph) ggc.uniqueResult();
    }


    public List<Integer> getGraphVariantOnly() {
        return sessionFactory.getCurrentSession().createCriteria(Graph.class)
                .setProjection(Projections.distinct(Projections.property("variant")))
                .addOrder(Property.forName("variant").asc()).list();
    }

    public DetailedVerdict checkCorrectness(String isDirected, String whatPresentation, int[][] etalonAdjMatrix, String presentedByUser, SubmitInfo submtInf, PropertyResourceBundle propertyResourceBundle)
    {
        int[][] userArray = new int[0][];
        try {
            userArray = My2DArrCoder.strTo2DArr(presentedByUser, propertyResourceBundle);
            if(whatPresentation.contains("ncidence")) {
                userArray = My2DArrCoder.incidenceMatrixToAdjMatrix(userArray, "true".equalsIgnoreCase(isDirected), propertyResourceBundle);
            } else if(whatPresentation.contains("Lists")) {
                userArray = My2DArrCoder.adjListsToAdjMatrix(userArray, propertyResourceBundle); // no matter directed or not
            } else {  // adjMatrix
                int numRows = userArray[0].length;
                for(int i=0; i<numRows; i++) {
                    if (userArray.length != userArray[i].length)
                        throw new IllegalArgumentException(propertyResourceBundle.getString("verdictExplain.incorrectAdjMatrixSize"));
                    for(int j=0; j<numRows; j++) {
                        if(userArray[i][j] < 0 || userArray[i][j] > 1) {
                            throw new IllegalArgumentException(String.format(propertyResourceBundle.getString("verdictExplain.illegalElementOfAdjMatrix"), i+1, j+1, userArray[i][j]));
                        }
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            submtInf.oneMoreTryWasted();
            return new DetailedVerdict(Verdict.PE, e.getMessage(), 0.0);
        }
        if(Arrays.deepEquals(userArray, etalonAdjMatrix)) {
            double score = submtInf.calcScore();
            return new DetailedVerdict(Verdict.OK, "Ok", score);
        } else {
            submtInf.oneMoreTryWasted();
            return new DetailedVerdict(Verdict.WA, My2DArrCoder.buildDetailedVerdict(userArray, etalonAdjMatrix, "true".equalsIgnoreCase(isDirected), propertyResourceBundle), 0.0);
        }
    }
}
