package com.springapp.mvc.data.services;

import com.springapp.mvc.data.entities.PointsUpdate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by ilya on 18.01.2016.
 */
@Repository
@Transactional
public class PointsUpdateService {
//    private static final Logger LOGGER = Logger.getLogger( PointsUpdateService.class.getName() );

    @Autowired
    SessionFactory sessionFactory;

    public void savePointsUpdate(PointsUpdate pointsUpdate) {
        sessionFactory.getCurrentSession().saveOrUpdate(pointsUpdate);
    }

    public List<PointsUpdate> getPointsUpdates() {
        return sessionFactory.getCurrentSession()
                .createCriteria(PointsUpdate.class)
                .list();
    }


}
