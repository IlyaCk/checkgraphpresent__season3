package com.springapp.mvc.data.services;

import com.springapp.mvc.GenOneTimeCode;
import com.springapp.mvc.data.entities.OneTimeCode;
import com.springapp.mvc.data.entities.Student;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ilya
 * Date: 15.04.14
 * Time: 11:40
 * To change this template use File | Settings | File Templates.
 */
@Repository
@Transactional
public class OneTimeCodeService {
//    private static final Logger LOGGER = Logger.getLogger( OneTimeCode.class.getName() );

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    GenOneTimeCode genOneTimeCode;

    @Autowired
    StudentService studentService;

    public void generateAndSave(Integer idStudent, String IP, int is4or5) {
        int codeItself;
        if(is4or5==4) {
            codeItself = GenOneTimeCode.genNew4();
        } else if(is4or5==5) {
            codeItself = GenOneTimeCode.genNew5();
        } else {
            throw new IllegalArgumentException("code length is incorrect (neither 4 nor 5)");
        }
        createOneTimeCode(codeItself, idStudent, IP);
    }

    protected void deletePrevCodesForStudentId(Integer idStudent, Integer codeToNotDelete) {
////        String hqlDelPrev = "delete from OneTimeCode oneTimeCode where oneTimeCode.idStudent = :idStudent and oneTimeCode.code != :theCode";
////        sessionFactory.getCurrentSession().createQuery(hqlDelPrev).setInteger("idStudent", idStudent).setInteger("theCode", theCode).executeUpdate();
        Session session = sessionFactory.getCurrentSession();
        Criteria crAllPrev = session.createCriteria(OneTimeCode.class);
        crAllPrev.add(Restrictions.eq("idStudent", idStudent));
        if(codeToNotDelete != null) {
            crAllPrev.add(Restrictions.ne("code", codeToNotDelete));
        }
        List<OneTimeCode> lstAllPrev = crAllPrev.list();
        for(OneTimeCode otc : lstAllPrev) {
            session.delete(otc);
        }
    }

    public void deleteAllByStudentId(Integer idStudent) {
        deletePrevCodesForStudentId(idStudent, null);
////        String hql = "delete from OneTimeCode oneTimeCode where oneTimeCode.idStudent = :idStudent";
////        sessionFactory.getCurrentSession().createQuery(hql).setInteger("idStudent", idStudent).executeUpdate();
    }

    public boolean checkIfCorrect(Integer idStudent, Integer theCode) {
////        String hqlCheck = "select oneTimeCode from OneTimeCode oneTimeCode where oneTimeCode.idStudent = :idStudent and oneTimeCode.code = :theCode";
////        List<OneTimeCode> lstOTCodes = sessionFactory.getCurrentSession().createQuery(hqlCheck).setInteger("idStudent", idStudent).setInteger("theCode", theCode).list();
        Criteria crSelByStudAndCode = sessionFactory.getCurrentSession().createCriteria(OneTimeCode.class);
        crSelByStudAndCode.add(Restrictions.eq("idStudent", idStudent));
        crSelByStudAndCode.add(Restrictions.eq("code", theCode));
        List<OneTimeCode> lstOTCodes = crSelByStudAndCode.list();
        if(lstOTCodes.isEmpty()) {
            return false;
        }
        long currTime = Calendar.getInstance().getTimeInMillis();
        boolean foundOkTime = false;
        for(OneTimeCode otc : lstOTCodes) {
            long timeBetween = currTime - otc.getTime().getTime();
            if(timeBetween > 0 && timeBetween < 300100) { // 5 min
                foundOkTime = true; break;
            }
        }
//        deletePrevCodesForStudentId(idStudent, foundOkTime ? theCode : null);
        deletePrevCodesForStudentId(idStudent, null);
        return foundOkTime;
    }

    public void saveOneTimeCode(OneTimeCode oneTimeCode) {
        deleteAllByStudentId(oneTimeCode.getIdStudent());
        sessionFactory.getCurrentSession().saveOrUpdate(oneTimeCode);
        Student selectedStudent = studentService.getStudent(oneTimeCode.getIdStudent());
        System.out.println("Student " + selectedStudent.getSurName() + " " + selectedStudent.getFirstName() + " (id=" + oneTimeCode.getIdStudent() + ") was assigned code " + oneTimeCode.getCode() + " at IP = " + oneTimeCode.getIP() + ", time = " + Calendar.getInstance().getTime().toString());
    }

    public OneTimeCode getOneTimeCode(Integer id) {
        return (OneTimeCode) sessionFactory.getCurrentSession().get(OneTimeCode.class, id);
    }

    public List<OneTimeCode> getOneTimeCodes() {
        return sessionFactory.getCurrentSession()
                .createCriteria(OneTimeCode.class)
                .list();
    }

    public void deleteOneTimeCode(OneTimeCode oneTimeCode) {
        sessionFactory.getCurrentSession().delete(oneTimeCode);
    }

    public void createOneTimeCode(int code, int idStudent, String IP) {
        OneTimeCode oneTimeCode = new OneTimeCode();
        oneTimeCode.setCode(code);
        oneTimeCode.setIdStudent(idStudent);
        oneTimeCode.setIP(IP);
        saveOneTimeCode(oneTimeCode);
    }
}
