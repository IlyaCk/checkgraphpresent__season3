package com.springapp.mvc.data.services;

import com.springapp.mvc.data.entities.PointsUpdate;
import com.springapp.mvc.data.entities.Student;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ilya
 * Date: 10.04.14
 * Time: 21:47
 * To change this template use File | Settings | File Templates.
 */
@Repository
@Transactional
public class StudentService {
//    private static final Logger LOGGER = Logger.getLogger( StudentService.class.getName() );

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    PointsUpdateService pointsUpdateService;

    public void saveStudent(Student student) {
        sessionFactory.getCurrentSession().saveOrUpdate(student);
    }

    public Student getStudent(Integer id) {
        return (Student) sessionFactory.getCurrentSession().get(Student.class, id);
    }

    public List<Student> getStudents() {
        return sessionFactory.getCurrentSession()
                .createCriteria(Student.class)
                .add(Restrictions.eq("isActive", 1))
                .list();
    }

    public List<Student> getStudentsSorted(Property propToSort) {
        return sessionFactory.getCurrentSession()
                .createCriteria(Student.class)
                .addOrder(propToSort.asc()).list();
    }

    public void deleteStudent(Student student) {
        sessionFactory.getCurrentSession().delete(student);
    }

    public void createStudent(String firstName, String surName, int variant,
              double unDirectedAdjListsScore, double unDirectedAdjMatrixScore, double unDirectedIncidenceMatrixScore,
              double yesDirectedAdjListsScore,double yesDirectedAdjMatrixScore,double yesDirectedIncidenceMatrixScore) {
        Student student = new Student();
        student.setFirstName(firstName);
        student.setSurName(surName);
        student.setVariant(variant);
        student.setUnDirectedAdjListsScore(unDirectedAdjListsScore);
        student.setUnDirectedAdjMatrixScore(unDirectedAdjMatrixScore);
        student.setUnDirectedIncidenceMatrixScore(unDirectedIncidenceMatrixScore);
        student.setYesDirectedAdjListsScore(yesDirectedAdjListsScore);
        student.setYesDirectedAdjMatrixScore(yesDirectedAdjMatrixScore);
        student.setYesDirectedIncidenceMatrixScore(yesDirectedIncidenceMatrixScore);
        saveStudent(student);
    }

    public Student getStudentH(Integer idStudent) {
        Criteria crGetSt = sessionFactory.getCurrentSession().createCriteria(Student.class);
        crGetSt.add(Restrictions.eq("idStudent", idStudent));
        return (Student) crGetSt.uniqueResult();
////        String hql = "select student from Student student where student.id = :idStudent";
////        return  (Student) sessionFactory.getCurrentSession().createQuery(hql)
////                .setInteger("idStudent", idStudent).uniqueResult();
    }

    /**
     * Loads from DB old score.
     * Should be called ONLY WHEN checked that all correct.
     *
     */
    public double loadScore(Integer idStudent, String isDirected, String whatPresentation, boolean isPermanent) throws IllegalArgumentException {
        Student theStudent = getStudent(idStudent);
        String allGrType = isDirected + whatPresentation + isPermanent;
        if("falseAdjListstrue".equalsIgnoreCase(allGrType)) {
            return theStudent.getUnDirectedAdjListsScore();
        } else if("falseAdjMatrixtrue".equalsIgnoreCase(allGrType)) {
            return theStudent.getUnDirectedAdjMatrixScore();
        } else if("falseIncidenceMatrixtrue".equalsIgnoreCase(allGrType)) {
            return theStudent.getUnDirectedIncidenceMatrixScore();
        } else if("trueAdjListstrue".equalsIgnoreCase(allGrType)) {
            return theStudent.getYesDirectedAdjListsScore();
        } else if("trueAdjMatrixtrue".equalsIgnoreCase(allGrType)) {
            return theStudent.getYesDirectedAdjMatrixScore();
        } else if("trueIncidenceMatrixtrue".equalsIgnoreCase(allGrType)) {
            return theStudent.getYesDirectedIncidenceMatrixScore();
        } else if("falseAdjListsfalse".equalsIgnoreCase(allGrType)) {
            return theStudent.getUnDirectedAdjListsNewNotProvedScore();
        } else if("falseAdjMatrixfalse".equalsIgnoreCase(allGrType)) {
            return theStudent.getUnDirectedAdjMatrixNewNotProvedScore();
        } else if("falseIncidenceMatrixfalse".equalsIgnoreCase(allGrType)) {
            return theStudent.getUnDirectedIncidenceMatrixNewNotProvedScore();
        } else if("trueAdjListsfalse".equalsIgnoreCase(allGrType)) {
            return theStudent.getYesDirectedAdjListsNewNotProvedScore();
        } else if("trueAdjMatrixfalse".equalsIgnoreCase(allGrType)) {
            return theStudent.getYesDirectedAdjMatrixNewNotProvedScore();
        } else if("trueIncidenceMatrixfalse".equalsIgnoreCase(allGrType)) {
            return theStudent.getYesDirectedIncidenceMatrixNewNotProvedScore();
        } else {
            throw new IllegalArgumentException("Unknown combination of isDirected = " + isDirected + ", whatPresentation = " + whatPresentation + " and isPermanent = " + isPermanent);
        }
    }

    public void clearTemporaryScoresWithLogging(Student theStudent) {
        theStudent.saveZeroAndLogIfWasNonZero("UnDirectedAdjLists");
        theStudent.saveZeroAndLogIfWasNonZero("UnDirectedAdjMatrix");
        theStudent.saveZeroAndLogIfWasNonZero("UnDirectedIncidenceMatrix");
        theStudent.saveZeroAndLogIfWasNonZero("YesDirectedAdjLists");
        theStudent.saveZeroAndLogIfWasNonZero("YesDirectedAdjMatrix");
        theStudent.saveZeroAndLogIfWasNonZero("YesDirectedIncidenceMatrix");
        saveStudent(theStudent);
//        LOGGER.log(Level.FINE, "All temp score were cleared for student " + theStudent.getSurName() + " " + theStudent.getFirstName());
    }

    public void clearTemporaryScores(Student theStudent) {
        theStudent.setUnDirectedAdjListsNewNotProvedScore(0.0);
        theStudent.setUnDirectedAdjMatrixNewNotProvedScore(0.0);
        theStudent.setUnDirectedIncidenceMatrixNewNotProvedScore(0.0);
        theStudent.setYesDirectedAdjListsNewNotProvedScore(0.0);
        theStudent.setYesDirectedAdjMatrixNewNotProvedScore(0.0);
        theStudent.setYesDirectedIncidenceMatrixNewNotProvedScore(0.0);
        saveStudent(theStudent);
//        LOGGER.log(Level.FINE, "All temp score were cleared for student " + theStudent.getSurName() + " " + theStudent.getFirstName());
    }

    protected void change(double[][] changes, int idx, Method getterOfPermanent, Method getterOfTemp, Method setterOfPermanent, String allGrType, Student theStudent, String remoteIP) throws InvocationTargetException, IllegalAccessException {
        changes[idx][0] = (double) getterOfPermanent.invoke(theStudent);
        double newValue = (double) getterOfTemp.invoke(theStudent);
        changes[idx][1] = Math.max(changes[idx][0], newValue);
        changes[idx][2] = changes[idx][1] - changes[idx][0];
        setterOfPermanent.invoke(theStudent, changes[idx][1]);
        if(changes[idx][2] > 0) {
            pointsUpdateService.savePointsUpdate(new PointsUpdate(theStudent, allGrType, changes[idx][0], changes[idx][1], remoteIP, true));
        }
    }

    public String makeTemporaryScoresPermanent(Student theStudent, String remoteIP) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        double[][] changes = new double[7][3];
        change(changes, 0, theStudent.getClass().getMethod("getUnDirectedAdjListsScore"),           theStudent.getClass().getMethod("getUnDirectedAdjListsNewNotProvedScore"),          theStudent.getClass().getMethod("setUnDirectedAdjListsScore",           Double.TYPE), "unDirectedAdjListsScore", 	theStudent, remoteIP);
        change(changes, 1, theStudent.getClass().getMethod("getUnDirectedAdjMatrixScore"),          theStudent.getClass().getMethod("getUnDirectedAdjMatrixNewNotProvedScore"),         theStudent.getClass().getMethod("setUnDirectedAdjMatrixScore",          Double.TYPE), "unDirectedAdjMatrixScore",	theStudent, remoteIP);
        change(changes, 2, theStudent.getClass().getMethod("getUnDirectedIncidenceMatrixScore"),    theStudent.getClass().getMethod("getUnDirectedIncidenceMatrixNewNotProvedScore"),   theStudent.getClass().getMethod("setUnDirectedIncidenceMatrixScore",    Double.TYPE), "unDirectedIncidenceMatrix",	theStudent, remoteIP);
        change(changes, 3, theStudent.getClass().getMethod("getYesDirectedAdjListsScore"),          theStudent.getClass().getMethod("getYesDirectedAdjListsNewNotProvedScore"),         theStudent.getClass().getMethod("setYesDirectedAdjListsScore",          Double.TYPE), "yesDirectedAdjListsScore",	theStudent, remoteIP);
        change(changes, 4, theStudent.getClass().getMethod("getYesDirectedAdjMatrixScore"),         theStudent.getClass().getMethod("getYesDirectedAdjMatrixNewNotProvedScore"),        theStudent.getClass().getMethod("setYesDirectedAdjMatrixScore",         Double.TYPE), "yesDirectedAdjMatrixScore",	theStudent, remoteIP);
        change(changes, 5, theStudent.getClass().getMethod("getYesDirectedIncidenceMatrixScore"),   theStudent.getClass().getMethod("getYesDirectedIncidenceMatrixNewNotProvedScore"),  theStudent.getClass().getMethod("setYesDirectedIncidenceMatrixScore",   Double.TYPE), "yesDirectedIncidenceMatrix",	theStudent, remoteIP);
        for(int i=0; i<6; i++) {
            for (int j=0; j<3; j++) {
                changes[6][j] += changes[i][j];
            }
        }
        StringBuilder allPointsChanges = new StringBuilder();
        for(int i=0; i<7; i++) {
            if(i==6) {
                allPointsChanges.append("------------------------\n");
            }
            allPointsChanges.append(String.format("%4.3f -> %4.3f  (+%4.3f)\n", changes[i][0], changes[i][1], changes[i][2]));
        }
        saveStudent(theStudent);
        return allPointsChanges.toString();
    }

    /**
     * Saves score.
     * Should be called ONLY WHEN checked that all correct.
     *
     */
    public void saveScore(Integer idStudent, String isDirected, String whatPresentation, boolean isPermanent, double score, double oldScore, String remoteIP) throws IllegalArgumentException {
        Student theStudent = getStudent(idStudent);
        String allGrType = isDirected + whatPresentation + isPermanent;
        if("falseAdjListstrue".equalsIgnoreCase(allGrType)) {
            theStudent.setUnDirectedAdjListsScore(score);
        } else if("falseAdjMatrixtrue".equalsIgnoreCase(allGrType)) {
            theStudent.setUnDirectedAdjMatrixScore(score);
        } else if("falseIncidenceMatrixtrue".equalsIgnoreCase(allGrType)) {
            theStudent.setUnDirectedIncidenceMatrixScore(score);
        } else if("trueAdjListstrue".equalsIgnoreCase(allGrType)) {
            theStudent.setYesDirectedAdjListsScore(score);
        } else if("trueAdjMatrixtrue".equalsIgnoreCase(allGrType)) {
            theStudent.setYesDirectedAdjMatrixScore(score);
        } else if("trueIncidenceMatrixtrue".equalsIgnoreCase(allGrType)) {
            theStudent.setYesDirectedIncidenceMatrixScore(score);
        } else if("falseAdjListsfalse".equalsIgnoreCase(allGrType)) {
            theStudent.setUnDirectedAdjListsNewNotProvedScore(score);
        } else if("falseAdjMatrixfalse".equalsIgnoreCase(allGrType)) {
            theStudent.setUnDirectedAdjMatrixNewNotProvedScore(score);
        } else if("falseIncidenceMatrixfalse".equalsIgnoreCase(allGrType)) {
            theStudent.setUnDirectedIncidenceMatrixNewNotProvedScore(score);
        } else if("trueAdjListsfalse".equalsIgnoreCase(allGrType)) {
            theStudent.setYesDirectedAdjListsNewNotProvedScore(score);
        } else if("trueAdjMatrixfalse".equalsIgnoreCase(allGrType)) {
            theStudent.setYesDirectedAdjMatrixNewNotProvedScore(score);
        } else if("trueIncidenceMatrixfalse".equalsIgnoreCase(allGrType)) {
            theStudent.setYesDirectedIncidenceMatrixNewNotProvedScore(score);
        } else {
            throw new IllegalArgumentException("Unknown combination of isDirected = " + isDirected + " and whatPresentation = " + whatPresentation);
        }
        saveStudent(theStudent);
        pointsUpdateService.savePointsUpdate(new PointsUpdate(theStudent, allGrType, oldScore, score, remoteIP, false));
    }
}
