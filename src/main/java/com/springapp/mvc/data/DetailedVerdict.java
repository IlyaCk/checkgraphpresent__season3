package com.springapp.mvc.data;

/**
 * Created with IntelliJ IDEA.
 * User: ilya
 * Date: 22.04.14
 * Time: 1:26
 * To change this template use File | Settings | File Templates.
 */
public class DetailedVerdict {
    Verdict mainVerdict;
    String DetailedExplain;
    double score;

    public DetailedVerdict(Verdict mainVerdict, String detailedExplain, double score) {
        this.mainVerdict = mainVerdict;
        DetailedExplain = detailedExplain;
        this.score = score;
    }

    public Verdict getMainVerdict() {
        return mainVerdict;
    }

    public String getDetailedExplain() {
        return DetailedExplain;
    }

    public double getScore() {
        return score;
    }
}
