package com.springapp.mvc.data.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by ilya on 18.01.2016.
 */
@Entity
@Table(name="PointsUpdate")
public class PointsUpdate {
    public PointsUpdate() {
    }

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "idStudent")
    int idStudent;

    @Column(name = "studentName")
    String studentName;

    @Column(name = "time")
    Timestamp time;

    @Column(name = "whatPresentation")
    String whatPresentation;

    @Column(name = "oldvalue")
    double oldvalue;

    @Column(name = "newvalue")
    double newvalue;

    @Column(name = "IP")
    String IP;

    @Column(name = "isPermanent")
    Boolean isPermanent;

    public PointsUpdate(Student theStudent, String whatPresentation, double oldvalue, double newvalue, String IP, Boolean isPermanent) {
        this.idStudent = theStudent.getId();
        this.studentName = theStudent.getSurName() + " " + theStudent.getFirstName() + " (" + theStudent.getGrp() + ")";
        this.whatPresentation = whatPresentation;
        this.oldvalue = oldvalue;
        this.newvalue = newvalue;
        this.IP = IP;
        Timestamp timestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
        this.time = timestamp;
        this.isPermanent = isPermanent;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp when) {
        this.time = when;
    }

    public String getWhatPresentation() {
        return whatPresentation;
    }

    public void setWhatPresentation(String whatpresentation) {
        this.whatPresentation = whatpresentation;
    }

    public double getOldvalue() {
        return oldvalue;
    }

    public void setOldvalue(double oldvalue) {
        this.oldvalue = oldvalue;
    }

    public double getNewvalue() {
        return newvalue;
    }

    public void setNewvalue(double newvalue) {
        this.newvalue = newvalue;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public Boolean getPermanent() {
        return isPermanent;
    }

    public void setPermanent(Boolean permanent) {
        isPermanent = permanent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
