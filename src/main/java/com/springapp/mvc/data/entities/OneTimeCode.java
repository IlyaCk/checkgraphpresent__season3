package com.springapp.mvc.data.entities;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: ilya
 * Date: 15.04.14
 * Time: 11:41
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="OneTimeCode")
public class OneTimeCode {
    public OneTimeCode() {
    }

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "code")
    private int code;

    @Column(name = "idStudent")
    private int idStudent;

    @Column(name = "IP")
    private String IP;

    @Column(name = "time")
    private Timestamp time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public Timestamp getTime() {
        return time;
    }
}
