package com.springapp.mvc.data.entities;

import javax.persistence.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * User: ilya
 * Date: 09.04.14
 * Time: 10:29
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="student")
public class Student {
//    private static final Logger LOGGER = Logger.getLogger( Student.class.getName() );

    public Student() {
    }

    public Student(String surName, String firstName, String grp, int variant) {
        this.surName = surName;
        this.firstName = firstName;
        this.grp = grp;
        this.variant = variant;
        this.setUnDirectedIncidenceMatrixScore(0.0);
        this.setUnDirectedAdjMatrixScore(0.0);
        this.setUnDirectedAdjListsScore(0.0);
        this.setYesDirectedIncidenceMatrixScore(0.0);
        this.setYesDirectedAdjMatrixScore(0.0);
        this.setYesDirectedAdjListsScore(0.0);
        this.setUnDirectedIncidenceMatrixNewNotProvedScore(0.0);
        this.setUnDirectedAdjMatrixNewNotProvedScore(0.0);
        this.setUnDirectedAdjListsNewNotProvedScore(0.0);
        this.setYesDirectedIncidenceMatrixNewNotProvedScore(0.0);
        this.setYesDirectedAdjMatrixNewNotProvedScore(0.0);
        this.setYesDirectedAdjListsNewNotProvedScore(0.0);
        this.isActive = 1;

    }

    @Id
    @GeneratedValue
    @Column(name = "idstudent")
    private int id;

    @Column(name = "surName")
    String surName;

    @Column(name = "firstName")
    String firstName;

    @Column(name = "grp")
    String grp;

    @Column(name = "variant")
    int variant;

    @Column(name = "unDirectedAdjMatrScore")
    double unDirectedAdjMatrixScore;

    @Column(name = "unDirectedIncidenceMatrScore")
    double unDirectedIncidenceMatrixScore;

    @Column(name = "unDirectedAdjListsScore")
    double unDirectedAdjListsScore;

    @Column(name = "yesDirectedAdjMatrScore")
    double yesDirectedAdjMatrixScore;

    @Column(name = "yesDirectedIncidenceMatrScore")
    double yesDirectedIncidenceMatrixScore;

    @Column(name = "yesDirectedAdjListsScore")
    double yesDirectedAdjListsScore;

    @Column(name = "unDirectedAdjMatrNewNotProvedScore")
    double unDirectedAdjMatrixNewNotProvedScore;

    @Column(name = "unDirectedIncidenceMatrNewNotProvedScore")
    double unDirectedIncidenceMatrixNewNotProvedScore;

    @Column(name = "unDirectedAdjListsNewNotProvedScore")
    double unDirectedAdjListsNewNotProvedScore;

    @Column(name = "yesDirectedAdjMatrNewNotProvedScore")
    double yesDirectedAdjMatrixNewNotProvedScore;

    @Column(name = "yesDirectedIncidenceMatrNewNotProvedScore")
    double yesDirectedIncidenceMatrixNewNotProvedScore;

    @Column(name = "yesDirectedAdjListsNewNotProvedScore")
    double yesDirectedAdjListsNewNotProvedScore;

    @Column(name = "isActive")
    int isActive;

    public int isActive() {
        return isActive;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGrp() {
        return grp;
    }

    public void setGrp(String grp) {
        this.grp = grp;
    }

    public int getVariant() {
        return variant;
    }

    public void setVariant(int variant) {
        this.variant = variant;
    }

    public double getUnDirectedAdjMatrixScore() {
        return unDirectedAdjMatrixScore;
    }

    public void setUnDirectedAdjMatrixScore(double unDirectedAdjMatrixScore) {
        this.unDirectedAdjMatrixScore = unDirectedAdjMatrixScore;
    }

    public double getUnDirectedIncidenceMatrixScore() {
        return unDirectedIncidenceMatrixScore;
    }

    public void setUnDirectedIncidenceMatrixScore(double unDirectedIncidenceMatrixScore) {
        this.unDirectedIncidenceMatrixScore = unDirectedIncidenceMatrixScore;
    }

    public double getUnDirectedAdjListsScore() {
        return unDirectedAdjListsScore;
    }

    public void setUnDirectedAdjListsScore(double unDirectedAdjListsScore) {
        this.unDirectedAdjListsScore = unDirectedAdjListsScore;
    }

    public double getYesDirectedAdjMatrixScore() {
        return yesDirectedAdjMatrixScore;
    }

    public void setYesDirectedAdjMatrixScore(double yesDirectedAdjMatrixScore) {
        this.yesDirectedAdjMatrixScore = yesDirectedAdjMatrixScore;
    }

    public double getYesDirectedIncidenceMatrixScore() {
        return yesDirectedIncidenceMatrixScore;
    }

    public void setYesDirectedIncidenceMatrixScore(double yesDirectedIncidenceMatrixScore) {
        this.yesDirectedIncidenceMatrixScore = yesDirectedIncidenceMatrixScore;
    }

    public double getYesDirectedAdjListsScore() {
        return yesDirectedAdjListsScore;
    }

    public void setYesDirectedAdjListsScore(double yesDirectedAdjListsScore) {
        this.yesDirectedAdjListsScore = yesDirectedAdjListsScore;
    }

    public double getUnDirectedAdjMatrixNewNotProvedScore() {
        return unDirectedAdjMatrixNewNotProvedScore;
    }

    public void setUnDirectedAdjMatrixNewNotProvedScore(Double unDirectedAdjMatrixNewNotProvedScore) {
        this.unDirectedAdjMatrixNewNotProvedScore = unDirectedAdjMatrixNewNotProvedScore;
    }

    public double getUnDirectedIncidenceMatrixNewNotProvedScore() {
        return unDirectedIncidenceMatrixNewNotProvedScore;
    }

    public void setUnDirectedIncidenceMatrixNewNotProvedScore(Double unDirectedIncidenceMatrixNewNotProvedScore) {
        this.unDirectedIncidenceMatrixNewNotProvedScore = unDirectedIncidenceMatrixNewNotProvedScore;
    }

    public double getUnDirectedAdjListsNewNotProvedScore() {
        return unDirectedAdjListsNewNotProvedScore;
    }

    public void setUnDirectedAdjListsNewNotProvedScore(Double unDirectedAdjListsNewNotProvedScore) {
        this.unDirectedAdjListsNewNotProvedScore = unDirectedAdjListsNewNotProvedScore;
    }

    public double getYesDirectedAdjMatrixNewNotProvedScore() {
        return yesDirectedAdjMatrixNewNotProvedScore;
    }

    public void setYesDirectedAdjMatrixNewNotProvedScore(Double yesDirectedAdjMatrixNewNotProvedScore) {
        this.yesDirectedAdjMatrixNewNotProvedScore = yesDirectedAdjMatrixNewNotProvedScore;
    }

    public double getYesDirectedIncidenceMatrixNewNotProvedScore() {
        return yesDirectedIncidenceMatrixNewNotProvedScore;
    }

    public void setYesDirectedIncidenceMatrixNewNotProvedScore(Double yesDirectedIncidenceMatrixNewNotProvedScore) {
        this.yesDirectedIncidenceMatrixNewNotProvedScore = yesDirectedIncidenceMatrixNewNotProvedScore;
    }

    public double getYesDirectedAdjListsNewNotProvedScore() {
        return yesDirectedAdjListsNewNotProvedScore;
    }

    public void setYesDirectedAdjListsNewNotProvedScore(Double yesDirectedAdjListsNewNotProvedScore) {
        this.yesDirectedAdjListsNewNotProvedScore = yesDirectedAdjListsNewNotProvedScore;
    }

    public double getTotalScore() {
        return unDirectedAdjListsScore + unDirectedAdjMatrixScore + unDirectedIncidenceMatrixScore +
                yesDirectedAdjListsScore + yesDirectedAdjMatrixScore + yesDirectedIncidenceMatrixScore;
    }

    public void saveZeroAndLogIfWasNonZero(String fieldName) {
        try {
            Method getter = this.getClass().getMethod("get" + fieldName + "NewNotProvedScore");
            Method setter = this.getClass().getMethod("set" + fieldName + "NewNotProvedScore", Double.class);
            double res = (Double) getter.invoke(this);
            if(res > 0.0) {
                System.err.println("field " + fieldName + " was set to zero (was " + res + ") for student " + surName + " " + firstName);
//                LOGGER.log(Level.INFO, "field " + fieldName + " was set to zero for student " + surName + " " + firstName);
            }
            setter.invoke(this, 0.0);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
//            LOGGER.log(Level.SEVERE, "NoSuchMethodException for getter or setter with filedName = " + fieldName);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
//            LOGGER.log(Level.SEVERE, "InvocationTargetException for getter or setter with filedName = " + fieldName);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
//            LOGGER.log(Level.SEVERE, "IllegalAccessException for getter or setter with filedName = " + fieldName);
        }
    }

}
