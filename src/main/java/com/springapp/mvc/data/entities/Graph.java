package com.springapp.mvc.data.entities;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: ilya
 * Date: 15.04.14
 * Time: 11:41
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="Graph")
public class Graph {
    public Graph() {
    }

    @Id
    @Column(name = "variant")
    private int variant;

    @Column(name = "unDirectedPictFileName")
    private String unDirectedPictFileName;

    @Column(name = "directedPictFileName")
    private String directedPictFileName;

    @Column(name = "unDirectedEthaloneAnswer")
    private String unDirectedEthaloneAnswer;

    @Column(name = "directedEthaloneAnswer")
    private String directedEthaloneAnswer;

    public int getVariant() {
        return variant;
    }

    public void setVariant(int variant) {
        this.variant = variant;
    }

    public String getUnDirectedPictFileName() {
        return unDirectedPictFileName;
    }

    public void setUnDirectedPictFileName(String unDirectedPictFileName) {
        this.unDirectedPictFileName = unDirectedPictFileName;
    }

    public String getDirectedPictFileName() {
        return directedPictFileName;
    }

    public void setDirectedPictFileName(String directedPictFileName) {
        this.directedPictFileName = directedPictFileName;
    }

    public String getUnDirectedEthaloneAnswer() {
        return unDirectedEthaloneAnswer;
    }

    public void setUnDirectedEthaloneAnswer(String unDirectedEthaloneAnswer) {
        this.unDirectedEthaloneAnswer = unDirectedEthaloneAnswer;
    }

    public String getDirectedEthaloneAnswer() {
        return directedEthaloneAnswer;
    }

    public void setDirectedEthaloneAnswer(String directedEthaloneAnswer) {
        this.directedEthaloneAnswer = directedEthaloneAnswer;
    }
}
