<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="language" value="${not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<html lang="${language}">
<head>
    <title>
        Втрачено авторизацію або доступ заборонено
    </title>
    <meta http-equiv="refresh" content="30; URL=/choosePresentation" />
</head>
<body>
    <script type="text/javascript" src="js/jquery-1.8.3.js"></script>
    <script language="javascript">
        function invalidate() {
            location = "/invalidateSession";
        }
//
//        function oneStepBack() {
//            setInterval(oneStepBack, 3600000); // 1 hour, actually means "never"
//            location.reload(history.go(-1));
//        }
//
//        function tryOneStepBack() {
//            $("div#divInfoItself").hide("slow");
//            setInterval(oneStepBack, 1000); // after 1 sec delay, actually retry
//        }
//
        function toPresentationChoice() {
            location = "choosePresentation";
        }
    </script>
    <h2>
        Втрачено авторизацію або доступ заборонено
    </h2>
    <div id = "divInfoItself">
        <p>
            Сталося одне з двох: або втрачено авторизацію, або Ви спробували перейти на сторінку, доступ до якої Вам заборонений.
        </p>
        <p>
            Одна з можливих причин &mdash; спроба вводити щось самостійно у рядок адреси. Будь ласка, не робіть так.
        </p>
        <p>
            Натисніть одну з двох кнопок:
            <button onClick = "invalidate()">розпочати заново з самого початку</button>,
            або
            <button onClick = "toPresentationChoice()">спробувати продовжити з місця вибору способу подання</button>
            (є шанси не втратити авторизованість)<!--,
            або
            <button onClick = "tryOneStepBack()">спробувати повернутися на один крок назад</button> (що мало в яких ситуаціях можливо)-->.
        </p>
    </div>
</body>
</html>
