<%--
  Created by IntelliJ IDEA.
  User: ilya
  Date: 12.04.14
  Time: 19:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : \"undefined\"}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<fmt:message key="chooseStudent.variant" />: <c:out value = "${sessionScope.graph.variant}"></c:out>
<c:choose>
    <c:when test = "${sessionScope.isConfirmed == 'true'}">
        <c:choose>
            <c:when test = "${sessionScope.randomOrPreDefined == 'random'}">
                (<fmt:message key="isAuthorized.randomVariant" />).
            </c:when>
            <c:otherwise>
                (<fmt:message key="isAuthorized.preDefinedVariant" />).
            </c:otherwise>
        </c:choose>
        <fmt:message key="isAuthorized.youAreAuthorizedAs" />
        <b>
            <c:out value = "${selectedStudentAsPojo.surName}"></c:out>
            <c:out value = "${selectedStudentAsPojo.firstName}"></c:out><%--
    --%></b>.
    </c:when>
    <c:otherwise>
        &nbsp;&nbsp;&nbsp;<fmt:message key="isAuthorized.youAreNotAuthorized" />
    </c:otherwise>
</c:choose>

<hr>