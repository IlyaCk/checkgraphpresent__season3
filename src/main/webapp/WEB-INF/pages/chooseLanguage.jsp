<%--
  Created by IntelliJ IDEA.
  User: ilya
  Date: 27.01.2017
  Time: 22:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Start page</title>
    </head>
    <body>
        <noscript>
            <p>
                <font color = "red">
                    Схоже, у Вашому браузері вимкнено javaScript.
                    Якщо це справді так, система не працюватиме.
                    Спробуйте увімкнути javaScript або зайти іншим браузером.
                </font>
            </p>
            <p>
                <font color = "red">
                    Похоже, в Вашем браузере отключен javaScript.
                    Если это действительно так, система не будет работать.
                    Попробуйте включить javaScript или зайти другим браузером.
                </font>
            </p>
            <p>
                <font color = "red">
                    It look like javaScript is switched off in your browser.
                    If it's really so, the system will not work.
                    Try to turn on javaScript or to use another browser.
                </font>
            </p>
        </noscript>
        <script type="text/javascript">
            function chooseLang(lang) {
                location = "/chooseStudent?language="+lang;
            }
        </script>
        <p>
            <button id="LangIsUkr" onclick="chooseLang('uk')">
                <font size="7">
                    Вибрати українську мову інтерфейсу
                </font>
            </button>
        </p>
        <p>
            <button id="LangIsRus" onclick="chooseLang('ru')">Выбрать русский язык интерфейса</button>
        </p>
        <p>
            <button id="LangIsEng" onclick="chooseLang('en')">Choose English interface language</button>
        </p>
        <p>
            <a href="/downloadAllResults" target="_blank">Скачати поточний стан усіх результатів усіх студентів</a>
        </p>
    </body>
</html>
