<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.util.Calendar" %>
<%--
  Created by IntelliJ IDEA.
  User: ilya
  Date: 12.04.14
  Time: 19:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : \"undefined\"}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<script language="javascript">
    function reCalcScoreIfNow() {
        $.ajax({
            url : '/getScoreIfNowOk',
            type: 'post',
            data : {
                isDirected: '${param.isDirected}',
                whatPresentation: '${param.whatPresentation}'
            },
            success: function(result, status, jqXHR) {
                if(result.hasOwnProperty("sessionBreaked")) {
                    $("div#divSubmitInfoEtc").hide("slow");
                    alert(('<fmt:message key="scoreIfNowOk.sessionIsPermanentlyLostOne" />'
                    <c:if test = "${sessionScope.isConfirmed == 'true'}">
                        + '<fmt:message key="scoreIfNowOk.sessionIsPermanentlyLostTwo" />'
                    </c:if>
                    ).replace(/<ptbr>/g, '\n'));
                    location = "/invalidateSession";
                } else {
                    $("div#divTryToReconnect").hide("slow");
                    $("button#buttonSubmit").attr("disabled", false);
                    $("label#numBadTries").text(result.numBadTries);
                    $("label#timePassed").text("" + Math.round(+result.timePassed / 1000));
                    $("label#timeNorm").text(result.timeNorm * 60);
                    $("label#scoreForNow").text(result.scoreForNow.toPrecision(3));
                    $("label#scoreMaxPossible").text(result.scoreMaxPossible);
                    $("div#divSubmitInfoEtc").show("slow");
                }
            },
            error: function(result, status, jqXHR) {
                $("div#divSubmitInfoEtc").hide("slow");
                $("div#divTryToReconnect").show("slow");
                $("button#buttonSubmit").attr("disabled", true);
            }
        })
    }

    function tryRestore() {
        $("div#divTryToReconnect").hide("slow");
    }

    setInterval(reCalcScoreIfNow, 20000);
    //setInterval(reCalcScoreIfNow, 123456789);
</script>

<hr>

<div id = "divSubmitInfoEtc" hidden = "true">
    <fmt:message key="scoreIfNowOk.badTries" /> <label id = "numBadTries"></label>
    <br>
    <fmt:message key="scoreIfNowOk.timeElapsed" />
    <br>
    <fmt:message key="scoreIfNowOk.scoreIfNowCalculated" />
</div>
<div id = "divTryToReconnect" hidden = "true">
    <p style = "color:red">
        <fmt:message key="scoreIfNowOk.noConnectionShort" />
    </p>
    <p style = "color:red">
        <fmt:message key="scoreIfNowOk.noConnectionDetailed" />
    </p>
    <button onClick="tryRestore()"><fmt:message key="scoreIfNowOk.tryToReconnect" /></button>
</div>
