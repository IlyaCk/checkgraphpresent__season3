<%--
  Created by IntelliJ IDEA.
  User: ilya
  Date: 12.04.14
  Time: 19:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />


<div id = "dontReloadWarning" hidden = "true">
    <p style = "color:red">
        <fmt:message key="dontReload.dontReload" />
    </p>
    <hr>
</div>

