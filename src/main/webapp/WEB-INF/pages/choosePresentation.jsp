<%--
  Created by IntelliJ IDEA.
  User: ilya
  Date: 09.04.14
  Time: 11:14
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<html lang="${language}">
    <head>
        <title>
            Вибір способу подання
        </title>
        <script type="text/javascript" src="js/jquery-1.8.3.js"></script>
        <script type="text/javascript" src="js/chooseStudFuncs.js"></script>
        <script type="text/javascript" src="js/startAndSave.js"></script>
    </head>
    <body>
        <%--<c:out value="${language}" />--%>
        <%@ include file="header.jsp" %>
        <%@ include file="isAuthorizedInfo.jsp" %>
        <%--<c:out value="${language}" />--%>
        <h2>
            <fmt:message key = "choosePresentation.choosePresentation" />
        </h2>
        <%--<c:out value="${language}" />--%>
        <table border = "1">
            <tr>
                <td>
                    <img src = "gif/${graph.unDirectedPictFileName}" >
                </td>
                <td>
                    <img src = "gif/${graph.directedPictFileName}" >
                </td>
            </tr>
            <tr>
                <td>
                    <button onClick = "startTest('false', 'adjLists')"><fmt:message key = "choosePresentation.undirAdjLists" /></button>
                    <c:if test = "${sessionScope.isConfirmed == 'true'}">
                        <br>(<fmt:message key = "choosePresentation.scoreGainedBefore" /> <c:out value = "${selectedStudentAsPojo.unDirectedAdjListsScore}"/>)
                        <br>(<fmt:message key = "choosePresentation.scoreGainedNow" /> <c:out value = "${selectedStudentAsPojo.unDirectedAdjListsNewNotProvedScore}"/>)
                    </c:if>
                </td>
                <td>
                    <button onClick = "startTest('true', 'adjLists')"><fmt:message key = "choosePresentation.dirAdjLists" /></button>
                    <c:if test = "${sessionScope.isConfirmed == 'true'}">
                        <br>(<fmt:message key = "choosePresentation.scoreGainedBefore" /> <c:out value = "${selectedStudentAsPojo.yesDirectedAdjListsScore}"/>)
                        <br>(<fmt:message key = "choosePresentation.scoreGainedNow" /> <c:out value = "${selectedStudentAsPojo.yesDirectedAdjListsNewNotProvedScore}"/>)
                    </c:if>
                </td>
            </tr>
            <tr>
                <td>
                    <button onClick = "startTest('false', 'adjMatrix')"><fmt:message key = "choosePresentation.undirAdjMatrix" /></button>
                    <c:if test = "${sessionScope.isConfirmed == 'true'}">
                        <br>(<fmt:message key = "choosePresentation.scoreGainedBefore" /> <c:out value = "${selectedStudentAsPojo.unDirectedAdjMatrixScore}"/>)
                        <br>(<fmt:message key = "choosePresentation.scoreGainedNow" /> <c:out value = "${selectedStudentAsPojo.unDirectedAdjMatrixNewNotProvedScore}"/>)
                    </c:if>
                </td>
                <td>
                    <button onClick = "startTest('true', 'adjMatrix')"><fmt:message key = "choosePresentation.dirAdjMatrix" /></button>
                    <c:if test = "${sessionScope.isConfirmed == 'true'}">
                        <br>(<fmt:message key = "choosePresentation.scoreGainedBefore" /> <c:out value = "${selectedStudentAsPojo.yesDirectedAdjMatrixScore}"/>)
                        <br>(<fmt:message key = "choosePresentation.scoreGainedNow" /> <c:out value = "${selectedStudentAsPojo.yesDirectedAdjMatrixNewNotProvedScore}"/>)
                    </c:if>
                </td>
            </tr>
            <tr>
                <td>
                    <button onClick = "startTest('false', 'incidenceMatrix')"><fmt:message key = "choosePresentation.undirIncMatrix" /></button>
                    <c:if test = "${sessionScope.isConfirmed == 'true'}">
                        <br>(<fmt:message key = "choosePresentation.scoreGainedBefore" /> <c:out value = "${selectedStudentAsPojo.unDirectedIncidenceMatrixScore}"/>)
                        <br>(<fmt:message key = "choosePresentation.scoreGainedNow" /> <c:out value = "${selectedStudentAsPojo.unDirectedIncidenceMatrixNewNotProvedScore}"/>)
                    </c:if>
                </td>
                <td>
                    <button onClick = "startTest('true', 'incidenceMatrix')"><fmt:message key = "choosePresentation.dirIncMatrix" /></button>
                    <c:if test = "${sessionScope.isConfirmed == 'true'}">
                        <br>(<fmt:message key = "choosePresentation.scoreGainedBefore" /> <c:out value = "${selectedStudentAsPojo.yesDirectedIncidenceMatrixScore}"/>)
                        <br>(<fmt:message key = "choosePresentation.scoreGainedNow" /> <c:out value = "${selectedStudentAsPojo.yesDirectedIncidenceMatrixNewNotProvedScore}"/>)
                    </c:if>
                </td>
            </tr>
        </table>
        <c:choose>
            <c:when test = "${sessionScope.isConfirmed == 'true'}">
                <p>
                    <fmt:message key = "choosePresentation.scoreChoosingMaxExplain" />
                </p>
                <p>
                    <button onClick = "startSaving()"><fmt:message key = "choosePresentation.finallyFinish" /></button>
                </p>
            </c:when>
            <c:otherwise>
                <a href = "/invalidateSession">
                    <fmt:message key = "choosePresentation.goToBegin" />
                </a>
            </c:otherwise>
        </c:choose>
    </body>
</html>
