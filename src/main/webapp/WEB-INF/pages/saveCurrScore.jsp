<%--
  Created by IntelliJ IDEA.
  User: ilya
  Date: 28.01.2017
  Time: 17:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<html lang="${language}">
<head>
    <title>
        Збереження результатів
    </title>
    <script type="text/javascript" src="js/jquery-1.8.3.js"></script>
    <script type="text/javascript" src="js/saveCurrScoreFuncs.js"></script>
</head>
<body>
    <%@ include file="header.jsp" %>
    <%@ include file="isAuthorizedInfo.jsp" %>
    <form>
        <div id = "divEnterCode">
            <p>
                <label id = "labelInputCodeAdress">
                </label>
                <label id = "labelInputCode">
                    <fmt:message key="saveCurrScore.enterNewCode" />
                </label>
                <input id = "inputCode" autocomplete="off" width="5"/>
            </p>
            <p>
                <button id = "buttonConfirmCode" type=submit formaction = "javascript:sendCodeTwo('<fmt:message key="saveCurrScore.codeShouldBe5Digit" />')"><fmt:message key="saveCurrScore.saveCodeRequired" /></button>
            </p>
            <p>
                <button id = "buttonGoBack" type=button onClick = "backToChoosePresentation()"><fmt:message key="saveCurrScore.backToChoosePresentation" /></button>
            </p>
        </div>
    </form>

</body>
</html>
