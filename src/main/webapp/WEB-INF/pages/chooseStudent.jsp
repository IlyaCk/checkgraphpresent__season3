<%--
  Created by IntelliJ IDEA.
  User: ilya
  Date: 09.04.14
  Time: 11:14
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : \"undefined\"}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<html lang="${language}">
    <head>
        <title>
            Вибір студента чи варіанту
        </title>
        <script type="text/javascript" src="js/jquery-1.8.3.js"></script>
        <script type="text/javascript" src="js/chooseStudFuncs.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("div#divSelectStudent").show();
                $("div#divEnterCode").hide();
                $("div#divSelectAnonymousVariant").hide();
            });
        </script>
    </head>
    <body>
        <%@ include file="header.jsp" %>
        <%@ include file="checksOfBrowser.jsp" %>

        <div id = "divSelectStudent">
            <h2>
                <fmt:message key="chooseStudent.chooseYourselfHeader" />
            </h2>
            <p>
                <fmt:message key="chooseStudent.chooseYourselfDetails" />
            </p>
            <p>
                <select name = "selectStudent" id = "selectStudent">
                    <c:forEach var = "theStudent"  items = "${allStudents}">
                        <option value = "${theStudent.id}">
                            <c:out value = "${theStudent.surName}"/>
                            <c:out value = "${theStudent.firstName}"/>
                            (<c:out value = "${theStudent.grp}"/>)
                        </option>
                    </c:forEach>
                </select>
                <button id = "buttonSelectStudent" onClick = "chooseStudent('<fmt:message key="chooseStudentJS.ifYouAreNot" />', '<fmt:message key="chooseStudentJS.clickHere" />')"><fmt:message key="chooseStudent.itsMeButton" /></button>
            </p>
            <p>
                <button id="startAnonymous" onClick = "showAnonymousVariants()"><fmt:message key="chooseStudent.tryAnonymouslyButton" /></button>
            </p>
        </div>
        <div id = "divEnterCode" hidden = "true">
            <p>
                <label id = "labelInputCodeAdress">
                </label>
                <label id = "labelInputCode">
                    <%--
                                        <c:out value="${selectedStudentAsPojo.surname}"/>
                                        <c:out value="${selectedStudentAsPojo.surname}"/>!
                                        !!! It DOESNOT work. It still doesn't exist at the moment when jsp translation is applied
                    --%>
                    <fmt:message key="chooseStudent.enterCode" />
                </label>
                <input id = "inputCode" autocomplete="off" width="5"/>
            </p>
            <p>
                <button id = "buttonConfirmCodePreDefinded" onClick = "sendCodeOne('preDefined', '<fmt:message key="chooseStudentJS.codeShouldBe4Digit" />')"><fmt:message key="chooseStudent.wantPreDefined" /></button>
            </p>
            <p>
                <button id = "buttonConfirmCodeRandom" onClick = "sendCodeOne('random', '<fmt:message key="chooseStudentJS.codeShouldBe4Digit" />')"><fmt:message key="chooseStudent.wantRandom" /></button>
            </p>
            <p>
                <label id = "labelGoBack">
                    <fmt:message key="chooseStudent.itsNotMe" />
                </label>
                <button id = "buttonGoBack" onClick = "itsNotMe()"><fmt:message key="chooseStudent.backToChooseStudent" /></button>
            </p>
        </div>
        <div id="divSelectAnonymousVariant" hidden="true">
            <p>
                <button id = "buttonReturnToChoice" onClick = "itsNotMe()"><fmt:message key="chooseStudent.backToChooseStudent" /></button>
            </p>
            <p>
                <label id="labelAnonymousVariant">
                    <fmt:message key="chooseStudent.chooseVariant" />
                </label>
                <select name = "selectAnonymousVariant" id = "selectAnonymousVariant">
                    <c:forEach var = "theVariant"  items = "${allGraphVariants}">
                        <option value = "${theVariant}">
                            <fmt:message key="chooseStudent.variant" /> <c:out value = "${theVariant}"/>
                        </option>
                    </c:forEach>
                </select>
<!--
                <script language="javascript">
                    $("#selectAnonymousVariant").change(function() {
                        $("#selectAnonymousVariant").val($("#selectAnonymousVariant").val());
                        // this was an unsuccessful try to fix bug of incorrect appearance in Android browser
                    })
                </script>
-->
                <button id = "buttonAnonymousVariant" onClick = "setAnonymousVariant()"><fmt:message key="chooseStudent.startAnonymous" /></button>
            </p>
        </div>
        <p>
            <a href="/downloadAllResults" target="_blank"><fmt:message key="chooseStudent.downloadAllResults" /></a>
        </p>

    </body>
</html>
