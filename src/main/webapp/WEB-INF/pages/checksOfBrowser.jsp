<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<noscript>
    <p>
        <font color = "red">
            Схоже, у Вашому браузері вимкнено javaScript.
            Якщо це справді так, система не працюватиме.
            Спробуйте увімкнути javaScript або зайти іншим браузером.
        </font>
    </p>
    <p>
        <font color = "red">
            Похоже, в Вашем браузере отключен javaScript.
            Если это действительно так, система не будет работать.
            Попробуйте включить javaScript или зайти другим браузером.
        </font>
    </p>
    <p>
        <font color = "red">
            It look like javaScript is switched off in your browser.
            If it's really so, the system will not work.
            Try to turn on javaScript or to use another browser.
        </font>
    </p>
</noscript>

<script>
    if(!navigator.cookieEnabled) {
        alert("Схоже, у Вашому браузері не підтримуються cookies.\nЯкщо це справді так, система не працюватиме.\nСпробуйте зайти іншим браузером або увімкнути cookies тут.")
    }
    if(typeof jQuery == 'undefined') {
        alert("Схоже, у Вашому браузері не підтримується jQuery.\nЯкщо це справді так, система не працюватиме.\nСпробуйте зайти іншим браузером або оновити цей.")
    }
    if (/Chrome[\/\s](\d+\.\d+)/.test(navigator.userAgent)){
        var version=new Number(RegExp.$1) // capture x.x portion and store as a number
        if (version<=3)
            alert("Схоже, Ви використовуєте занадто старий браузер Chrome " + version + ".\nЯкщо це справді так, система не працюватиме.\nСпробуйте зайти іншим браузером або оновити цей.")
    }
    if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)){
        var version=new Number(RegExp.$1) // capture x.x portion and store as a number
        if (version<=3)
            alert("Схоже, Ви використовуєте занадто старий браузер Firefox " + version + ".\nЯкщо це справді так, система не працюватиме.\nСпробуйте зайти іншим браузером або оновити цей.")
    }
</script>

