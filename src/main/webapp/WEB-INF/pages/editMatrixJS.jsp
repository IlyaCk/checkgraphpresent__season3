<%--
  Created by IntelliJ IDEA.
  User: ilya
  Date: 30.01.2017
  Time: 13:16
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<script type="text/javascript">
    $.extend(
        {
            redirectPost: function(location, args)
            {
                var form = '';
                $.each( args, function( key, value ) {
                    value = value.split('"').join('\"')
                    form += '<input type="hidden" name="'+key+'" value="'+value+'">';
                });
                $('<form action="' + location + '" method="POST">' + form + '</form>').appendTo($(document.body)).submit();
            }
        });

    function dec_N_rows() {
        if((+document.getElementById("N_rows").value) > 1) {
            document.getElementById("N_rows").value -= 1;
            cellChange('M_cols','N_rows');
        }
    }

    function inc_N_rows() {
        if((+document.getElementById("N_rows").value) < 17) {
            document.getElementById("N_rows").value -= (-1);
            cellChange('M_cols','N_rows');
        }
    }

    function dec_M_cols() {
        if((+document.getElementById("M_cols").value) > 1) {
            document.getElementById("M_cols").value -= 1;
            cellChange('N_rows','M_cols');
        }
    }

    function inc_M_cols() {
        if((+document.getElementById("M_cols").value) < 17) {
            document.getElementById("M_cols").value -= (-1);
            cellChange('N_rows','M_cols');
        }
    }

    function intToStr(i, j) {
        return "" + j;
    }

    function inputTagWithIndice(i, j, doFillWithZero) {
        return "<input id = \"matrix-cell-" + i + "-" + j + "\" size = \"1\" value = \"" + (doFillWithZero ? "0" : "") + "\" onChange = \" cellChange('matrix-cell-" + j + "-" + i + "', 'matrix-cell-" + i + "-" + j + "')\">";
    }

    function AddOrRemoveCells(tbl, i, mmm, funcIdxToHTML, doFillWithZero) {
        if(mmm < tbl.rows[i].cells.length-1) {
            for(var j=tbl.rows[i].cells.length-1; j>mmm; j--) {
                tbl.rows[i].deleteCell(j);
            }
        }
        else {
            for(var j=tbl.rows[i].cells.length; j<=mmm; j++) {
                if(j >= tbl.rows[i].cells.length) {
                    tbl.rows[i].insertCell(j);
                    tbl.rows[i].cells[j].innerHTML = funcIdxToHTML(i, j, doFillWithZero);
                }
            }
        }
    }

    function setOneOfSizes(id, name, max) {
        var xxx = +document.getElementById(id).value;
        if (isNaN(xxx) || xxx < 1) {
            alert("<fmt:message key="editMatrix.quantity" />" + " " + name + " " + "<fmt:message key="editMatrix.shouldBeIntPos" />");
            return null;
        }
        if(xxx > max) {
            alert("<fmt:message key="editMatrix.tooBigQuantity" />" + " " + name + " " + xxx + " " + "<fmt:message key="editMatrix.tooBigQuantity" />" + " " + max);
            xxx = max;
            document.getElementById(id).value = nnn;
        }
        return xxx;
    }

    function setMatrixSizes() {
        return setMatrixSizes(false)
    }

    function setMatrixSizes(skipConfirmDelete) {
        var nnn = setOneOfSizes("N_rows", "<fmt:message key="editMatrix.rows" />", 17);
        if(nnn==null || isNaN(nnn)) {
            return;
        }
        var mmm = setOneOfSizes("M_cols", "<fmt:message key="editMatrix.columns" />", 17);
        if(mmm==null || isNaN(mmm)) {
            return;
        }
        $("#buttonSetMatrixSizes").prop("value", "<fmt:message key="editMatrix.changeSizes" />");
        $("button#buttonSubmit").show("slow");
        $("#doFillZeroes").hide("slow");
        $("#dontReloadWarning").show("slow");
        $("#divTranspose").show("slow");
        if (nnn != mmm) {
            $("div#doSymmetric").hide("slow");
        }
        $("#divForMatrixItself").show("slow");
        var tbl = document.getElementById("tableForMatrixItself");
        if(nnn+1 < tbl.rows.length   ||
            tbl.rows.length > 1 && mmm+1 < tbl.rows[0].cells.length)
        {
            if(!skipConfirmDelete) {
                if( ! confirm("<fmt:message key="editMatrix.reallyDeleteExistingCells" />")) {
                    return;
                }
            }
        }
        AddOrRemoveCells(tbl, 0, mmm, intToStr);
        var doFillWithZero = document.getElementsByName("howInitMatrix")[0].checked;
        if(nnn < tbl.rows.length-1) {
            for(var i=tbl.rows.length-1; i>nnn; i--) {
                tbl.deleteRow(i);
            }
        }
        for(var i=1; i<=nnn; i++) {
            if(i >= tbl.rows.length) {
                tbl.insertRow(i);
                tbl.rows[i].insertCell(0);
                tbl.rows[i].cells[0].innerHTML = "" + i;
            }
            AddOrRemoveCells(tbl, i, mmm, inputTagWithIndice, doFillWithZero);
        }
    }

    function cellChange(a, b) {
        if(document.getElementById("checkboxDoSymmetric").checked) {
            document.getElementById(a).value = document.getElementById(b).value;
        }
        else {
            $("div#doSymmetric").hide("slow");
        }
    }

    function transponeWholeMatrix() {
        var tbl = document.getElementById("divForMatrixItself").children[0];
        var arrayContent = new Array(nnn);
        var nnn = tbl.rows.length - 1;
        var mmm = tbl.rows[0].cells.length - 1;
        for(var i=0; i<nnn; i++) {
            arrayContent[i] = new Array(mmm);
            for(var j=0; j<mmm; j++) {
                arrayContent[i][j] = tbl.rows[i + 1].cells[j + 1].children[0].value;
            }
        }
        document.getElementById("N_rows").value = "" + mmm;
        document.getElementById("M_cols").value = "" + nnn;
        setMatrixSizes(true);
        for(var i=0; i<mmm; i++) {
            for(var j=0; j<nnn; j++) {
                tbl.rows[i + 1].cells[j + 1].innerHTML = "<input id = \"matrix-cell-" + i + "-" + j + "\" size = \"1\" value = \"" +
                arrayContent[j][i] +
                "\" onChange = \" cellChange('matrix-cell-" + j + "-" + i + "', 'matrix-cell-" + i + "-" + j + "')\">";
    //            alert("i = " + i + "\nj = " + j + "\nvalue arrayContent[j][i] = " + arrayContent[j][i] + "\nwas(?) just assigned to tbl.rows[" + (i + 1) + "].cells[" + (j + 1) + "]");
            }
        }
    }































    function clearAllEmptyFromConcreteList(i) {
        var lists = document.getElementById("divForListsThemselves").children[0];
        for (var j = lists.rows[i].cells.length - 1; j > 3; j--) {
            if (lists.rows[i].cells[j].children[0].value == "") {
                lists.rows[i].deleteCell(j);
            }
        }
    }

    function incSizeByOne(i) {
        var lists = document.getElementById("divForListsThemselves").children[0];
        var len = lists.rows[i].cells.length;
        if(len < 17 + 4) {
            lists.rows[i].insertCell(len);
            lists.rows[i].cells[len].innerHTML = '<input size = "1" value = ""/>';
        }
    }

    function setListsQuantity() {
        var nnn = +($("input#N_lists").val());
        var lists = document.getElementById("divForListsThemselves").children[0];
        if(nnn > lists.rows.length) {
            for(var i=0; i<nnn; i++) {
                if (i >= lists.rows.length) {
                    lists.insertRow(i);
                    lists.rows[i].insertCell(0);
                    lists.rows[i].cells[0].innerHTML = "<button title = '<fmt:message key="editMatrix.clearEmptyCells" />' onClick = 'clearAllEmptyFromConcreteList(" + i + ")'>&lt;&lt;</button>";
                    lists.rows[i].insertCell(1);
                    lists.rows[i].cells[1].innerHTML = "<button title = '<fmt:message key="editMatrix.addOneCell" />' onClick = 'incSizeByOne(" + i + ")'>&gt;</button>";
                    lists.rows[i].insertCell(2);
                    lists.rows[i].cells[2].innerHTML = "" + ((+i) + 1);
                    lists.rows[i].insertCell(3);
                    lists.rows[i].cells[3].innerHTML = ":";
                }
            }
        } else if(nnn < lists.rows.length) {
            var someNonEmptyFound = false;
            for(var i=nnn; i < lists.rows.length; i++) {
                clearAllEmptyFromConcreteList(i);
                if(lists.rows[i].cells.length > 4) {
                    someNonEmptyFound = true;
                }
            }
            if(!someNonEmptyFound || confirm("<fmt:message key="editMatrix.reallyDeleteExistingLists" />".replace("[placeholder1]", (nnn+1)).replace("[placeholder2]", lists.rows.length))) {
                for(var i=lists.rows.length-1; i>=nnn; i--) {
                    lists.deleteRow(i);
                }
            }
        } else {
            alert("<fmt:message key="editMatrix.reallyDeleteExistingLists" />".replace("[placeholder1]", nnn));
        }
        $("div#dontReloadWarning").show("slow");
        $("div#divForListsThemselves").show("slow");
        $("button#buttonSubmit").show("slow");
        $("#buttonSetListsQuantity").prop("value", "<fmt:message key="editMatrix.changeSizes" />");
    }

    function dec_N_lists() {
        var nnn = $("input#N_lists").val();
        if(nnn > 1) {
            $("input#N_lists").val(nnn - 1);
        }
    }

    function inc_N_lists() {
        var nnn = $("input#N_lists").val();
        if(nnn < 17) {
            $("input#N_lists").val(nnn - (-1));
        }
    }

























    function submitToServer(isDirected, whatPresentation) {
        if(whatPresentation.indexOf("Matrix") >= 0) {
            var tbl = document.getElementById("divForMatrixItself").children[0];
            var startRow = 1;
            var startColumn = 1;
        } else {
            var tbl = document.getElementById("divForListsThemselves").children[0];
            var startRow = 0;
            var startColumn = 4;
        }
        var presentedByUser = "";
        for(var i=startRow; i<tbl.rows.length; i++) {
            if(whatPresentation.indexOf("Lists")>=0) {
                presentedByUser += "-1c";
            }
            for(var j=startColumn; j<tbl.rows[i].cells.length; j++) {
                if(!tbl.rows[i].cells[j].children[0].value.match("^[\\+\\-]?\\d+$")) {

                    alert("<fmt:message key="editMatrix.thisCellHasIncorrectFormat" />".replace("[placeholder1]", (i+1-startRow)).replace("[placeholder2]", (j+1-startColumn)).replace(/<ptbr>/g, "\n"));
                    return;
                }
                presentedByUser += tbl.rows[i].cells[j].children[0].value + "c";
            }
            presentedByUser += 'rrr';
        }
        $.ajax({
            url: 'theMainCheckStage',
            type: 'post',
            data: {
                isDirected : isDirected,
                whatPresentation : whatPresentation,
                presentedByUser	: presentedByUser,
                doShowErrorDetails : $("#inputCheckboxDoShowErrorDetails").is(":checked")
            },
            success : function(result, status, jqXHR) {
                if(result.hasOwnProperty("verdict")) {
                    if(result.verdict == "OK") {
                        reCalcScoreIfNow();
                        var msgOk = "<fmt:message key="editMatrix.hurraCorrect" />" + "\n\n";
                        if(result.isConfirmed == "true") {
                            var scoreOld = parseFloat(result.scoreOldProved);
                            if(parseFloat(result.scoreOldNotProved) > scoreOld) {
                                scoreOld = parseFloat(result.scoreOldNotProved);
                            }
                            msgOk += "<fmt:message key="editMatrix.detalizeScoreForOfficial" />".replace("[placeholder1]", (scoreOld.toFixed(3))).replace("[placeholder2]", (parseFloat(result.scoreNew).toFixed(3))).replace("[placeholder3]", (parseFloat(result.scoreMaxPossible).toFixed(3))).replace(/<ptbr>/g, "\n");
                            if(result.scoreNew > scoreOld) { // TODO: continue from here
                                msgOk += "\n\n" + "<fmt:message key="editMatrix.ieIncreasedBy" />" + " " + (parseFloat(result.scoreNew) - scoreOld).toFixed(3);
                            } else {
                                msgOk += "\n\n" + "<fmt:message key="editMatrix.ieNotIncreased" />";
                            }
                        } else {
                            msgOk += "<fmt:message key="editMatrix.detalizeScoreForNonOfficial" />".replace("[placeholder1]", (result.scoreNew)).replace("[placeholder2]", (result.scoreMaxPossible)).replace(/<ptbr>/g, "\n");
                        }
                        alert(msgOk);
                        location = "/choosePresentation";
                    } else {
                        if(result.verdict == "PE" || result.verdict == "WA") {
                            mess = result.verdict == "PE" ?
                                    "<fmt:message key="editMatrix.notGraphAtAll" />" :
                                    "<fmt:message key="editMatrix.notTheGraphNeeded" />";
                            mess += "\n\n<fmt:message key="editMatrix.moreDetailed" />" + ": " + result.detailedExplain;
                            alert(mess);
                        } else {
                            alert("Щось не так із системою перевірки.\n\nНерозпізнаний вердикт ``" + result.verdict + "''.\nБудь ласка, покличте викладача, не закриваючи це повідомлення\n" + result + "\n" + result.verdict);
                            reCalcScoreIfNow();
                        }
                    }
                } else {
                    alert("Щось не так із системою перевірки.\n\najax-відповідь отримана, але не містить поля ``verdict''.\nБудь ласка, покличте викладача, не закриваючи це повідомлення\n" + result);
                }
            },
            error : function(result, status, jqXHR) {
                alert("ajax returned error\n" + result + "\n" + status + "\n" + jqXHR);
            }
        });
    }

</script>