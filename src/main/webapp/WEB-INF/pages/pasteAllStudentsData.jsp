<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <title>
            Paste students' data
        </title>
    </head>
    <body>
        <h2>
            Paste students' data
        </h2>
        <p>
            Paste students' data here. But don't forget to start from one-time code.
        </p>
        <form >
            <textarea name = "allStudentsData" rows = 20 cols = 50>

            </textarea>
            <input name = "qaz" type = "text" value = "Qaz"/>
            <button formaction = "/performAddingStudents">Submit data!</button>
        </form>
    </body>
</html>
