<%--
  Created by IntelliJ IDEA.
  User: ilya
  Date: 27.01.2017
  Time: 23:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<script type="text/javascript">
    function itsNotMe() {
        $("div#divSelectStudent").show("slow");
        $("div#divEnterCode").hide("slow");
        $("div#divSelectAnonymousVariant").hide("slow");
    }

    var theSelectedStudentId = null;

    var codeStoredOne = null;

/*
    // TODO: add another random code, generated in javascript at each chooseStudent() call, to pass as parameter and store in OneTimeCode DB. Це здається потрібним, бо так начебто можливо задовольнити двом дещо протирічивим вимогам: з одного боку наразі реалізоване "при аболютно кожній генерації нового коду робити недійсниими всі старі" має недолік, який можна б і справити: ХТОСЬ ІНШИЙ зробити недійсним чесно отриманий код. З іншого боку, якщо дозволяти просто генерувати їх багато штук -- гіпотетично можна встигнути нагенерувати сотні штук і пробувати вводити навмання.
*/

    function chooseStudent() {
        $.ajax({
            url: 'generateOneTimeCode',
            type: 'post',
            data: {
                selectedStudent : $("select#selectStudent").val()
            },
            success : function(result, status, jqXHR) {
                theSelectedStudentId = result.idStudent; // global for javascript (?)

                $("label#labelGoBack").text(<fmt:message key="chooseStudentJS.ifYouAreNot" /> + result.surname + " " + result.firstname + <fmt:message key="chooseStudentJS.clickHere" />);
                $("label#labelInputCodeAdress").text(result.surname + " " + result.firstname + ", ");
                $("div#divSelectStudent").hide("slow");
                $("div#divEnterCode").show("slow");
                $("div#divSelectAnonymousVariant").hide("slow");
            },
            error : function(result, status, jqXHR) {
                alert("Щось сурово не так у виклику genOTC!\nПокличте, будь ласка, викладача, не закриваючи дане повідомлення\n" + result + "\n" + result.idStudent + "\n" + status + "\n" + jqXHR);
            }
        });
    }

    function sendCodeOne(randomOrNot) {
        if(new RegExp("^\\d{4}$").test($("input#inputCode").val())) {
            $.ajax({
                url : '/checkOneTimeCodeOne',
                data : {
                    selectedStudent : theSelectedStudentId,
                    theCode : $("input#inputCode").val(),
                    randomOrPreDefined : randomOrNot
                },
                success : function(result, status, jqXHR) {
                    if(result.isConfirmed == "true") {
                        codeStoredOne = $("input#inputCode").val();
                        location = "/choosePresentation";
                    } else {
                        if(result.hasOwnProperty("msg")) {
                            alert(result.msg);
                        } else {
                            alert("ajax щось повернув, але щось сурово не те\nПокличте, будь ласка, викладача, не закриваючи дане повідомлення\n" + result + "\n" + status + "\n" + jqXHR);
                        }
                    }
                },
                error : function(result, status, jqXHR) {
                    alert("Щось сурово не так у виклику chkOTC\najax нічого не повернув\nПокличте, будь ласка, викладача, не закриваючи дане повідомлення\n" + result + "\n" + status + "\n" + jqXHR);
                }
            });

        } else {
            alert(<fmt:message key="chooseStudentJS.ifYouAreNot" />);
        }
    }

    function showAnonymousVariants() {
        $("div#divSelectStudent").hide("slow");
        $("div#divEnterCode").hide("slow");
        $("div#divSelectAnonymousVariant").show("slow");
    }

    $.extend(
        {
            redirectPost: function(location, args)
            {
                var form = '';
                $.each( args, function( key, value ) {
                    value = value.split('"').join('\"')
                    form += '<input type="hidden" name="'+key+'" value="'+value+'">';
                });
                $('<form action="' + location + '" method="POST">' + form + '</form>').appendTo($(document.body)).submit();
            }
        });

    function setAnonymousVariant() {
        $.redirectPost("/setAnonymousVariant", {selectAnonymousVariant : $("select#selectAnonymousVariant").val()});
    }
</script>