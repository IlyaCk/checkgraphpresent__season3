<%--
  Created by IntelliJ IDEA.
  User: ilya
  Date: 09.04.14
  Time: 11:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<html lang="${language}">
    <head>
        <title>
            Власне введення матриці/списків
        </title>
        <script type="text/javascript" src="js/jquery-1.8.3.js"></script>
        <script type="text/javascript" src="js/chooseStudFuncs.js"></script>
<%--
        <script type="text/javascript" src="js/startAndSave.js"></script>
--%>
    </head>
    <body>
        <%@ include file="editMatrixJS.jsp" %>
        <%@ include file="header.jsp" %>
        <%@ include file="isAuthorizedInfo.jsp" %>
        <%@ include file="dontReload.jsp" %>
        <h2>
            <c:choose>
                <c:when test = "${param.whatPresentation == 'incidenceMatrix'}">
                    <fmt:message key="buildPresentation.enterIncidenceMatrix" />
                </c:when>
                <c:when test = "${param.whatPresentation == 'adjMatrix'}">
                    <fmt:message key="buildPresentation.enterAdjMatrix" />
                </c:when>
                <c:when test = "${param.whatPresentation == 'adjLists'}">
                    <fmt:message key="buildPresentation.enterAdjLists" />
                </c:when>
                <c:otherwise>
                    Вводьте невідомо що (і взагалі &mdash; припиніть, будь ласка,
                    самостійно вводити неправильні параметри в рядок адреси;
                    ходіть по посиланням, і все працюватиме...)
                </c:otherwise>
            </c:choose>
        </h2>
        <table>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <c:choose>
                                    <c:when test = "${param.isDirected == 'true'}">
                                        <img id = "imgGraphDiagram" src = "gif/${graph.directedPictFileName}" >
                                    </c:when>
                                    <c:when test = "${param.isDirected == 'false'}">
                                        <img id = "imgGraphDiagram" src = "gif/${graph.unDirectedPictFileName}" >
                                    </c:when>
                                    <c:otherwise>
                                        <p>
                                            Тут мало бути зображення графа, але його чомусь нема.
                                        </p>
                                        <p>
                                            Якщо Ви вводили щось у рядок адреси вручну &mdash;
                                            просто не&nbsp;робіть так, переходячи лише по кнопкам та лінкам).
                                        </p>
                                        <p>
                                            Якщо це повідомлення виникло, хоча Ви переходили лише по кпонкам та лінкам &mdash;
                                            повідомте, будь ласка, викладачу.
                                        </p>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id = "divForCanvas" hidden = "hidden">
                                    <fmt:message key="buildPresentation.marksOverPictureOne" />
                                    <br>
                                    <div id = "divMouseDown">
                                        <fmt:message key="buildPresentation.marksOverPictureTwo" />
                                    </div>
                                    <div id = "divMouseUp" hidden = "hidden">
                                        <fmt:message key="buildPresentation.marksOverPictureThree" />
                                    </div>
                                    <br>
                                    <button id = "buttonReloadCanvasImage" onClick = "initAll()"><fmt:message key="buildPresentation.marksClear" /></button>
                                    <br>
                                    <canvas id = "canvasToDrawOverGraphDiagram">
                                    </canvas>
                                    <script>
                                        var canvasWithGraphDiagram;
                                        var isLastMouseDownEventActual = false;

                                        window.onload = initAll;

                                        function initAll() {
                                            try {
                                                canvasWithGraphDiagram = document.getElementById("canvasToDrawOverGraphDiagram");
                                                var graphDiagram = document.getElementById("imgGraphDiagram");
                                                var context = canvasWithGraphDiagram.getContext("2d");
                                                context.clearRect(0, 0, canvasWithGraphDiagram.width, canvasWithGraphDiagram.height);
                                                context.beginPath();
                                                context.drawImage(graphDiagram, 0, 0, graphDiagram.width, graphDiagram.height);
                                                canvasWithGraphDiagram.addEventListener("mousedown", saveCoords, true);
                                                canvasWithGraphDiagram.addEventListener("mouseup", drawSegm, true);
                                                canvasWithGraphDiagram.addEventListener("mouseout", drawSegm, true);
                                                $("#imgGraphDiagram").hide("slow");
                                                $("#divForCanvas").show("slow");
                                                $("#buttonReloadCanvasImage").attr("disabled", "disabled");
                                                isLastMouseDownEventActual = false;
                                            } finally {
                                                reCalcScoreIfNow();
                                            }
                                        }

                                        function saveCoords(event) {
                                            canvasWithGraphDiagram.getContext("2d").moveTo(event.offsetX, event.offsetY);
                                            $("#divMouseDown").hide();
                                            $("#divMouseUp").show();
                                            isLastMouseDownEventActual = true;
                                        }

                                        function drawSegm(event) {
                                            if(isLastMouseDownEventActual) {
                                                var context = canvasWithGraphDiagram.getContext("2d");
                                                context.lineTo(event.offsetX, event.offsetY);
                                                context.strokeStyle = "#00FF00";
                                                context.stroke();
                                                $("#divMouseDown").show();
                                                $("#divMouseUp").hide();
                                                $("#buttonReloadCanvasImage").removeAttr("disabled");
                                            }
                                            isLastMouseDownEventActual = false;
                                        }
                                    </script>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <c:if test="${fn:containsIgnoreCase(param.whatPresentation, 'Matrix')}">
                        <div id = "allSizesEtc">
                            <div id = "doSymmetric">
                                <input type = "checkbox" id = "checkboxDoSymmetric" name = "checkboxDoSymmetric"><label for = "checkboxDoSymmetric"><fmt:message key = "buildPresentation.autoSymmetric" /></label>
                            </div>
                            <div id = "divForSizes">
                                <table>
                                    <tr>
                                        <td><label for = "N_rows"><fmt:message key = "buildPresentation.numRows" />:</label></td>
                                        <td><input type = "button" title = "<fmt:message key = "buildPresentation.decrease" />" value = "&lt;" onClick = "dec_N_rows()"></td>
                                        <td><input id = "N_rows" name = "N" size = "2" value = "7" onChange = "cellChange('M_cols','N_rows')"></td>
                                        <td><input type = "button" title = "<fmt:message key = "buildPresentation.increase" />" value = "&gt;" onClick = "inc_N_rows()"></td>
                                    </tr>
                                    <tr>
                                        <td><label for = "M_cols"><fmt:message key = "buildPresentation.numCols" />:</label></td>
                                        <td><input type = "button" title = "<fmt:message key = "buildPresentation.decrease" />" value = "&lt;" onClick = "dec_M_cols()"></td>
                                        <td><input id = "M_cols" name = "M" size = "2" value = "7" onChange = "cellChange('N_rows','M_cols')"></td>
                                        <td><input type = "button" title = "<fmt:message key = "buildPresentation.increase" />" value = "&gt;" onClick = "inc_M_cols()"></td>
                                    </tr>
                                </table>
                                <table id = "doFillZeroes">
                                    <tr><td><fmt:message key = "buildPresentation.creatingMatrix" /></td><td>
                                        <input type = "radio" name = "howInitMatrix" value = "withZeroes" checked = "checked"><fmt:message key = "buildPresentation.fillZeroes" />
                                    </td></tr>
                                    <tr><td></td><td>
                                        <input type = "radio" name = "howInitMatrix" value = "emptyStrings"><fmt:message key = "buildPresentation.leaveEmpty" />
                                    </td></tr>
                                </table>
                                <input type = "button" id ="buttonSetMatrixSizes"
                                       name = "setSizes" value = "<fmt:message key = "buildPresentation.setSizes" />" onClick = "setMatrixSizes()">
                            </div>
                            <div id = "divTranspose" hidden = "hidden">
                                <input type = "button" id = "buttonTranspose"
                                        value = "<fmt:message key = "buildPresentation.transpose" />" onClick="transponeWholeMatrix()">
                            </div>
                        </div>
                        <div id = "divForMatrixItself" hidden = "hidden">
                            <table id = "tableForMatrixItself">
                                <tr><td></td></tr>
                            </table>
                        </div>
                    </c:if>
                    <c:if test="${fn:containsIgnoreCase(param.whatPresentation, 'Lists')}">
                        <div id = "listsSizesEtc">
                            <table>
                                <tr>
                                    <td><label for="N_lists"><fmt:message key="buildPresentation.numRows" />:</label></td>
                                    <td><input type = "button" title = "<fmt:message key="buildPresentation.decrease" />" value = "&lt;" onClick = "dec_N_lists()"></td>
                                    <td><input id = "N_lists" name = "N" size = "2" value = "7"></td>
                                    <td><input type = "button" title = "<fmt:message key="buildPresentation.increase" />" value = "&gt;" onClick = "inc_N_lists()"></td>
                                </tr>
                            </table>
                            <input type = "button" id ="buttonSetListsQuantity"
                                   name = "setSizes" value = "<fmt:message key="buildPresentation.setSizes" />" onClick = "setListsQuantity()">
                        </div>
                        <div id = "divForListsThemselves" hidden = "true">
                            <table>
                                <tbody></tbody> <%--But no rows/cells!!!--%>
                            </table>
                        </div>
                    </c:if>
                </td>
            </tr>
            <tr>
                <td>
                    <button id = "buttonSubmit" hidden = "true" onClick = "submitToServer('${param.isDirected}', '${param.whatPresentation}')"><fmt:message key="buildPresentation.submitNow" /></button>
                </td>
            </tr>
        </table>
        <input id = "inputCheckboxDoShowErrorDetails" type = "checkbox" value = "doShowErrorDetails"><label for = "inputCheckboxDoShowErrorDetails"><fmt:message key="buildPresentation.doDetalizeErrors" /></label>

        <%@include file="scoreIfNowOk.jsp"%>

        <hr>

        <a href="/choosePresentation"><fmt:message key="buildPresentation.backToChoosePresentationOne" /></a>
        <fmt:message key="buildPresentation.backToChoosePresentationTwo" />


    </body>
</html>
