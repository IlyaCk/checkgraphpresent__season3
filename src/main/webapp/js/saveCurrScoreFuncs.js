/**
 * Created by ilya on 28.01.2017.
 */
function sendCodeTwo(msgCodeShouldBe5Digit) {
    if(new RegExp("^\\d{5}$").test($("input#inputCode").val())) {
        $.ajax({
            url : '/checkOneTimeCodeTwo',
            data : {
                theCode : $("input#inputCode").val(),
            },
            success : function(result, status, jqXHR) {
                if(result.isConfirmed == "true") {
                    codeStoredOne = $("input#inputCode").val();
                    alert("Ok\n\n" + result.changes);
                    location = "/invalidateSession";
                } else {
                    if(result.hasOwnProperty("msg")) {
                        alert(result.msg);
                    } else {
                        alert("ajax щось повернув, але щось сурово не те\nПокличте, будь ласка, викладача, не закриваючи дане повідомлення\n" + result + "\n" + status + "\n" + jqXHR);
                    }
                }
            },
            error : function(result, status, jqXHR) {
                alert("Щось сурово не так у виклику chkOTC\najax нічого не повернув\nПокличте, будь ласка, викладача, не закриваючи дане повідомлення\n" + result + "\n" + status + "\n" + jqXHR);
            }
        });

    } else {
        alert(msgCodeShouldBe5Digit);
    }
}

function backToChoosePresentation() {
    location = "/choosePresentation"
}

$.extend(
    {
        redirectPost: function(location, args)
        {
            var form = '';
            $.each( args, function( key, value ) {
                value = value.split('"').join('\"')
                form += '<input type="hidden" name="'+key+'" value="'+value+'">';
            });
            $('<form action="' + location + '" method="POST">' + form + '</form>').appendTo($(document.body)).submit();
        }
    });
