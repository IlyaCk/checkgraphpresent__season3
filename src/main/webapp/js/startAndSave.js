/**
 * Created with IntelliJ IDEA.
 * User: ilya
 * Date: 18.04.14
 * Time: 1:47
 * To change this template use File | Settings | File Templates.
 */

$.extend(
    {
        redirectPost: function(location, args)
        {
            var form = '';
            $.each( args, function( key, value ) {
                value = value.split('"').join('\"')
                form += '<input type="hidden" name="'+key+'" value="'+value+'">';
            });
            $('<form action="' + location + '" method="POST">' + form + '</form>').appendTo($(document.body)).submit();
        }
    });

function startTest(isDirected, whatPresentation) {
    $.redirectPost("/buildThePresentation", {isDirected : isDirected, whatPresentation : whatPresentation});
}

function startSaving() {
    $.redirectPost("/startSaveCurrScore", {});
}


